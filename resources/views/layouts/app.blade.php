<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'MyRent') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script
    src="https://code.jquery.com/jquery-3.3.1.js"
    integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
    crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md">
            <div class="container">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <i class="fa fa-navicon"></i>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">

                    <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    @if(Auth::user()->user_type!=1)
                    <li class="nav-item">
                        <a class="nav-link navlink-custom ml-3 mr-3" href="{{route('dashboard')}}">Home</a>
                    </li>
                    @endif

                    @if(Auth::user()->user_type == 1)
                        <li class="nav-item">
                        <a class="nav-link navlink-custom ml-3 mr-3" href="{{route('admin.client')}}">Client</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link navlink-custom ml-3 mr-3" href="{{route('view.subs.pending')}}">Subscriptions</a>
                        </li>
                    @elseif(Auth::user()->user_type==2)                
                        <li class="nav-item">
                            <a class="nav-link navlink-custom ml-3 mr-3" href="{{route('landlord.room')}}">Rooms</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link navlink-custom ml-3 mr-3" href="{{route('landlord.tenant')}}">Tenants</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link navlink-custom ml-3 mr-3" href="{{route('landlord.bill')}}">Bills</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link navlink-custom ml-3 mr-3" href="{{route('landlord.records')}}">Records</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link navlink-custom ml-3 mr-3" href="{{route('view.message.landlord')}}">Messages</a>
                        </li>
                    @elseif(Auth::user()->user_type = 3)
                    <li class="nav-item">
                        <a class="nav-link navlink-custom ml-3 mr-3" href="{{route('view.bills')}}">Bills</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link navlink-custom ml-3 mr-3" href="{{route('view.message.tenant')}}">Messages</a>
                    </li>
                        @endif
                        <li class="nav-item nav-link dropdown">
                            <a id="navbarDropdown" class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                <img src="http://ei.is.tuebingen.mpg.de/assets/noEmployeeImage_md-eaa7c21cc21b1943d77e51ab00a5ebe9.png" alt="profile_picture" class="user-img" style="margin:0;padding:0">
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{route('profile')}}">{{ __('Profile') }}</a>
                                @if(auth()->user()->user_type==1)
                                    <a class="dropdown-item" href="{{route('admin.accounts')}}">{{ __('Admin Accounts') }}</a>
                                @endif
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <main style="position:relative">
            @yield('content')
        </main>
        <div class="container">
            @include('messages')
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    @yield('page_scripts')
</body>
</html>

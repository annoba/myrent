<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'MyRent') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
    body{
        background-color:#edf1f7;
    }
    </style>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="offset-lg-1 col-lg-10 text-center">
                <div id="index-content">
                    <div id="title-index">{{config('app.name','MyRent')}}</div>
                    <div id="desc-index">description dummy description dummy description dummy description dummy description dummy description dummy description dummy description dummy description dummy description dummy description dummy description dummy description dummy description dummy</div>
                    
                    <hr class="hr-custom mt-5">

                <form action="{{ route('login') }}" method="POST">
                        @csrf
                            <div class="form-group ">
                                <input type="text" name="usermail" class="center-obj input-custom-md form-control mt-4"  placeholder="Enter username or email" required>
                                @if ($errors->first()=='usermail')
                                    <small class="form-text text-muted error-msg">Username or email doesn't exist.</small>
                                @endif
                                <input type="password" name="password" class="center-obj input-custom-md form-control mt-3"  placeholder="Enter password" required>
                                @if ($errors->first()=='incorrectpass')
                                    <small class="form-text text-muted error-msg">Password is incorrect</small>
                                @endif
                                @if ($errors->first()=='toomanylog')
                                    <small class="form-text text-muted error-msg">Too many log in attempts, please try again after an hour</small>
                                @endif
                            </div>

                            <button type="submit" class="btn btn-custom">Submit</button>

                    </form>

                    <hr class="hr-custom mt-3">

                    <div class="center-obj col-lg-5">
                        <a href="">Forgot Password?</a> Don't have account yet? Be a partner. Sign up <a href="">here!</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</body>
</html>


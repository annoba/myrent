@if(count($errors)>0)
<div class='alert alert-danger alert-dismissible mt-8'>
	<div class="custom-msg text-center">
		@foreach($errors-> all() as $error)	
			<p><strong>{{$error}}</strong></p><br>
		@endforeach
	</div>
	<button type="button" class="close close-alert" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
</div>
@endif

@if(session('success'))
	<div class="alert alert-success alert-dismissible mt-4 custom-alert">
		<div class="custom-msg text-center">
			<strong>{{session('success')}}</strong>
		</div>
		<button type="button" class="close close-alert" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
	</div>
@endif

@if(session('error'))
	<div class="alert alert-danger alert-dismissible mt-4">
		<div class="custom-msg text-center">
			<strong>{{session('error')}}</strong>
		</div>
		<button type="button" class="close close-alert" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
	</div>
@endif

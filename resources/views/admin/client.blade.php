@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12 row" style="margin:0px;">
           <h3 class="font-weight-bold mb-0"> Clients</h3> &nbsp
           <h6 style="margin-top:10px;"><a href="{{route('add.client')}}">Add</a></h6>
        </div>
        
        <div class="col-sm-12">
        <h6>Number of Clients: {{count($data)}}</h6>
        </div>
        <div class="col-sm-12 mt-4">
            <table id="clientTable" class="table" >
                <thead class="table-header table-striped">
                    <tr>
                        <th class="text-center">Client ID</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">Contact Number</th>
                        <th class="text-center">Gender</th>
                        <th class="text-center">Birthdate</th>
                        <th class="text-center">Building Name</th>
                        {{-- <th class="text-center">Action</th> --}}
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $item)
                    <tr class="item{{$item->id}}">
                        <td class="table-light">{{$item->id}}</td>
                        <td class="table-light">{{$item->first_name}} {{$item->last_name}}</td>
                        <td class="table-light">{{$item->contact_number}}</td>
                        <td class="table-light">{{$item->gender}}</td>
                        <td class="table-light">{{$item->birthday}}</td>
                        <td class="table-light">{{$item->name}}</td>
                        {{-- <td class="table-light">
                        <button class="edit-modal btn btn-info" data-info="{{$item->id}},{{$item->first_name}},{{$item->last_name}}">
                                <span class="fa fa-pencil-square-o"></span> Edit
                        </button>
                        <button class="delete-modal btn btn-danger" data-info="{{$item->id}},{{$item->first_name}},{{$item->last_name}}">
                            <span class="fa fa-trash-o"></span> Delete
                        </button></td> --}}
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div> 
</div>
<script>
$(document).ready(function() {
    $('#clientTable').DataTable({
        'order':[[0,'desc']]
    });
});
</script>
@endsection

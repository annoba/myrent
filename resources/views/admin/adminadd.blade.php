@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="offset-lg-2  mt-5">
           <h1 class="title-tag">New Admin Account</h1>
        </div>
    </div>
    <div class="row">
        <div class="offset-lg-2 col-lg-6">
            <h6>Add new account</h6>
        </div>
    </div>

    <div class="row">
        <div class="offset-lg-2 col-lg-6">
        <form action="{{route('insert.account')}}" method="POST"> 
            @csrf

            <div class="form-group mt-4">
                <h5> Admin Personal Information </h5>
                <div class="row mt-2">
                    <div class="col-lg-6">
                    <label for="Firstname">First Name</label>
                        <input type="text" value="{{old('Firstname')}}" name="Firstname" class="form-control"  placeholder="Enter first name" required>
                    </div>
                    <div class="col-lg-6">
                    <label for="Lastname">Last Name</label>
                        <input type="text" value="{{old('Lastname')}}" name="Lastname" class="form-control"  placeholder="Enter last name" required>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-lg-6">
                    <label for="contnum">Contact Number</label>
                        <input type="text" maxlength="11" value="{{old('contnum')}}" name="contnum" class="form-control number"  placeholder="Enter contact number" required>
                    </div>
                </div>
            </div>

            <div class="form-group mt-4">
                <h5> Admin Login Information </h5>
                <div class="row mt-2">
                    <div class="col-lg-6">
                    <label for="username">Username</label>
                        <input type="text" value="{{old('username')}}" name="username" class="form-control"  placeholder="Enter username" required>
                    </div>
                    <div class="col-lg-6">
                    <label for="email">Email</label>
                        <input type="mail" value="{{old('mail')}}" name="mail" class="form-control"  placeholder="Enter email" required>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-lg-6">
                    <label for="password">Password</label>
                        <input type="password" name="password" class="form-control"  placeholder="Enter password" required>
                    </div>
                    <div class="col-lg-6">
                    <label for="password_confirmation">Confirm Password</label>
                        <input type="password" class="form-control"  name="password_confirmation" placeholder="Confirm password" required>
                    </div>
                </div>
            </div>

            

            <button type="submit" class="btn btn-primary btn-custom float-right">Submit</button>

        </form>
        </div>
    </div>
</div>
<script type="text/javascript">
$(function(){
    $('.number').keydown(function(e){
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    $('#radioBtn a').on('click', function(){
        var sel = $(this).data('title');
        var tog = $(this).data('toggle');
        $('#gender').prop('value', sel);

        $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
        $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
    });



});
</script>
@endsection

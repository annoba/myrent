@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
           <h1 class="title-tag"> Subscriptions</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
        <h6>Number of subscriptions: {{count($data)}} </h6>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-12">
            <table id="subsTable" class="table" >
                    <thead class="table-header table-striped">
                        <tr>
                            <th class="text-center">Room Number</th>
                            <th class="text-center">Landlord Name</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $item)
                        <tr class="item{{$item->id}}">
                            <td class="table-light">{{$item->room_number}}</td>
                            <td class="table-light">{{$item->first_name}} {{$item->last_name}}</td>
                            @if($item->status==1)
                            <td class="table-light">
                                <button class="accept-modal btn btn-info"
                                    data-id="{{$item->id}}"  style="margin-left:30%">
                                    <span class="fa fa-pencil-square-o"></span> Accept
                                </button>
                                <button class="denie-modal btn btn-danger"
                                    data-id="{{$item->id}}">
                                    <span class="fa fa-trash-o"></span>Denie
                                </button></td>
                            @else
                            <td class="table-light"></td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
            </table>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
    $('#subsTable').DataTable({
        'order':[[2,'desc']]
    });

    function requestCall($id,$action){
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url:'{{route("request.action.subs")}}',
            method:'post',
            data:{id:$id,action:$action},
            success:function(data){
                if(data=='success')
                {
                   location.reload();
                }
            }
        });
    }
    $('.accept-modal').on('click',function(){
        $id=this.getAttribute('data-id');
        requestCall($id,'accept')
    });
    $('.denie-modal').on('click',function(){
        $id=this.getAttribute('data-id');
        requestCall($id,'denie')
    });

});
</script>
@endsection

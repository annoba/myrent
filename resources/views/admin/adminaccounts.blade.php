@extends('layouts.app')

@section('content')
<div class="container-fluid mt-3 mb-10">
    <div class="row">
        <div class="offset-lg-2  mt-5">
           <h1 class="title-tag">Admin Accounts</h1>
        </div>
    <div class=" col-lg-2 mt-5"><a class="title-side" href="{{route('add.admin.acc')}}">Add account</a></div>
    </div>
    <div class="row">
        <div class="offset-lg-2 col-lg-6">
        <h6>Number accounts: {{count($data)}} </h6>
        </div>
    </div>
    <div class="row mt-5">
        <div class="offset-lg-2 col-lg-8">
            <table id="subsTable" class="table" >
                    <thead class="table-header table-striped">
                        <tr>
                            <th class="text-center">ID</th>
                            <th class="text-center">Name</th>
                            <th class="text-center">Username</th>
                            <th class="text-center">Email</th>
                            <th class="text-center">Contact Number</th>
                            <th class="text-center">Added By</th>
                            <th class="text-center">Active</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $item)
                        <tr class="item-row item{{$item->id}}">
                                <td class="table-light">{{$item->id}}</td>
                            <td class="table-light">{{$item->first_name}} {{$item->last_name}}</td>
                            <td class="table-light">{{$item->username}}</td>
                            <td class="table-light">{{$item->email}}</td>
                            <td class="table-light">{{$item->contact_number}}</td>
                            <td class="table-light">{{$item->added_by}}</td>
                            @if($item->isActive==1)
                                <td class="table-light">Active</td>
                            @else
                                <td class="table-light">Not-Active</td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
            </table>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
    $table=$('#subsTable').DataTable({
        'order':[[0,'desc']]
    });

    function requestCall($id,$action){
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url:'{{route("request.action.subs")}}',
            method:'post',
            data:{id:$id,action:$action},
            success:function(data){
                if(data=='success')
                {
                   location.reload();
                }
            }
        });
    }
    $('.accept-modal').on('click',function(){
        $id=this.getAttribute('data-id');
        requestCall($id,'accept')
    });
    $('.denie-modal').on('click',function(){
        $id=this.getAttribute('data-id');
        requestCall($id,'denie')
    });
});
</script>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12 row" style="margin:0px;">
        <h3 class="font-weight-bold mb-0">Pending Subscriptions</h3>  &nbsp
            <h6 style="margin-top:10px;"><a href="{{route('view.subs.all')}}">View all</a></h6>
        </div>
        <div class="col-sm-12">
        <h6>Number of pending subscriptions: {{count($data)}} </h6>
        </div>
        <div class="col-sm-12 mt-4">
            <table id="subsTable" class="table table-striped table-bordered">
                <thead class="table-header table-striped">
                    <tr>
                        <th class="text-center">Room Number</th>
                        <th class="text-center">Landlord Name</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $item)
                    <tr class="item-row item{{$item->id}}">
                        <td class="table-light">{{$item->room_number}}</td>
                        <td class="table-light">{{$item->first_name}} {{$item->last_name}}</td>
                        <td class="table-light">
                            <button class="accept-modal btn btn-info"
                                data-id="{{$item->id}}" style="margin-left:30%">
                                <span class="fa fa-pencil-square-o"></span> Accept
                            </button>
                            <button class="denie-modal btn btn-danger" data-id="{{$item->id}}">
                                <span class="fa fa-trash-o"></span>Denie
                            </button></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
    $('#subsTable').DataTable();

    function requestCall($id,$action){
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url:'{{route("request.action.subs")}}',
            method:'post',
            data:{id:$id,action:$action},
            success:function(data){
                if(data=='success')
                {
                   location.reload();
                }
            }
        });
    }
    $('.accept-modal').on('click',function(){
        $id=this.getAttribute('data-id');
        requestCall($id,'accept')
    });
    $('.denie-modal').on('click',function(){
        $id=this.getAttribute('data-id');
        requestCall($id,'denie')
    });
});
</script>
@endsection

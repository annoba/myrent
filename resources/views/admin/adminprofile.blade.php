@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="offset-lg-2  mt-5">
           <h1 class="title-tag">Profile Account</h1>
        </div>
    </div>
    <div class="row mt-5">
        <div class="offset-lg-2 col-lg-8">
            <form action="">
            <div class="row">
                <div class="col-lg-3">
                    <img src="http://ei.is.tuebingen.mpg.de/assets/noEmployeeImage_md-eaa7c21cc21b1943d77e51ab00a5ebe9.png" class="prof-photo" alt="prof image" height="250" width="250">
                </div>
                <div class="offset-lg-1 col-lg-4">
                    <div class="row">
                        <div class="col-lg-12">
                        <label for="Firstname">First Name</label>
                        <input type="text" value="{{$data->first()->first_name}}" name="Firstname" class="center-obj form-control"  placeholder="Enter first name" required>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-lg-12">
                        <label for="Lastname">Last Name</label>
                            <input type="text" value="{{$data->first()->last_name}}" name="Lastname" class="center-obj form-control"  placeholder="Enter last name" required>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-lg-12">
                        <label for="contnum">Contact Number</label>
                            <input type="text" maxlength="11" value="{{$data->first()->contact_number}}" name="contnum" class="form-control number"  placeholder="Enter contact number" required>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="row">
                        <div class="col-lg-12">
                            <label for="email">Email</label>
                            <input type="mail" value="{{$data->first()->email}}" name="mail" class="form-control"  placeholder="Enter email" required>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-lg-12">
                            <label for="username">Username</label>
                            <input type="text" value="{{$data->first()->username}}" name="username" class="form-control"  placeholder="Enter username" required>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-lg-12 mt-4">
                                <button type="submit" class="btn btn-primary btn-custom float-right">Update</button>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<script>
$(function(){
    $('.number').keydown(function(e){
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
});
</script>
@endsection

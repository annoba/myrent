@extends('layouts.app')

@section('content')
<div class="container mt-3">
    <div class="row">
        <div class="col-sm-12 row" style="margin:0px;">
           <h3 class="font-weight-bold mb-0"> Bills</h3> &nbsp
        </div>
        <div class="row col-sm-12">
            <div class="col-sm-3">
            <select class='form-control' id="bill_type-selection" class="custom-select" onChange="BillChange()">
            @if(count($data['bill_types']) == 0)
                    <option selected>No Bill found</option>
                @else
                    @foreach($data['bill_types'] as $bill_type)
                        <option value="{{$bill_type->id}}">{{$bill_type['type']}}</option>
                    @endforeach
            @endif
            </select>
            </div>
            <div class="col-sm-9">
            @if($data['room']['isPremium'] == 1)
                <button class="btn btn-success d-none" onClick="approveBill()" id="approve_btn"> Approve </button>
            @endif
            <!-- <button class="btn btn-link d-none" data-toggle="modal" data-target="#editBillModal" onClick="editBill()"id="edit_btn"> Edit </button> -->
            <table id="clientTable" class="table">
                <thead class="table-header table-striped">
                    <tr>
                        <th class="text-center">Status</th>
                        <th class="text-center">Inclusive Date</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">Amount</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>

            </div>
        </div>
    </div> 

    <div class="modal fade" id="editBillModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                <h4 class="modal-title"> Edit Bill </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form method="post" enctype="multipart/form-data" autocomplete="false" action="{{ url('/editBill') }}">
                @csrf
                <div class="modal-body col-sm-12" id='body-modal'>
                
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                    </form>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
var table = $('#clientTable').DataTable({
        'order':[[0,'desc']],
        "paging":   false,
        "info":     false
});
window.onload = function(){
    
    var query = $('#bill_type-selection').val();
    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url:"{{ route('tenant.getBills') }}",
        method:"POST",
        data:{
            query:query, _token: '<?php echo csrf_token() ?>',
        },
        success:function(data){
            console.log(data);
            var count = 0;
            for (var i = 0; i != data.length; i++){
                if(data[i].status == "Pending"){
                    count++;
                }
            }
            if(count != 0){
                var approve = document.getElementById('approve_btn');
                approve.classList.remove('d-none');
            }
            for (var i = 0; i != data.length; i++){
                table.row.add([
                    data[i].status,
                    data[i].inclusive_date,
                    data[i].billed_to,
                    data[i].amount
                ]).draw(false);
            }
        }            
    });

};

var total = 0;
var count = 0;

   function editBill(){
       total = 0;
       count = 0;

        var query = $('#bill_type-selection').val();
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url:"{{ route('tenant.getPendingBills') }}",
            method:"POST",
            data:{
                query:query,_token: '<?php echo csrf_token() ?>',
            },
            success:function(data){
                console.log(data);
                count = data.length;
                var body = document.getElementById('body-modal');
                var content = "";
                for(var i=0;i!= data.length;i++){
                    content += '<p class="mt-2">' + data[i].billed_to+ '</p>';
                    content += '<input type="number" name="bill[i]" "value='+data[i].amount+' class="form-control edit-bill" inclusive_date="'+data[i].inclusive_date+'">';
                    total += data[i].amount;
                }
                body.innerHTML= content;

            }            
        });

    };

    $('input[type=number]').keyup(function(){

    });

    function approveBill(){
        var query = $('#bill_type-selection').val();
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url:"{{ route('tenant.approvePendingBill') }}",
            method:"POST",
            data:{
                query:query,_token: '<?php echo csrf_token() ?>',
            },
            success:function(data){
                console.log(data);
                var approve = document.getElementById('approve_btn');
                approve.classList.add('d-none');
                var query = $('#bill_type-selection').val();
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url:"{{ route('tenant.getBills') }}",
                    method:"POST",
                    data:{
                        query:query, _token: '<?php echo csrf_token() ?>',
                    },
                    success:function(data){
                        console.log(data);
                        table.rows().remove().draw();
                        var count = 0;
                        for (var i = 0; i != data.length; i++){
                            if(data[i].status == "Pending"){
                                count++;
                            }
                        }
                        if(count != 0){
                            var approve = document.getElementById('approve_btn');
                            approve.classList.remove('d-none');
                        }
                        for (var i = 0; i != data.length; i++){
                            table.row.add([
                                data[i].status,
                                data[i].inclusive_date,
                                data[i].billed_to,
                                data[i].amount
                            ]).draw(false);
                        }
                    }
                });
            }            
        });
    }
    
    function BillChange(){
        var query = $('#bill_type-selection').val();
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url:"{{ route('tenant.getBills') }}",
            method:"POST",
            data:{
                query:query, _token: '<?php echo csrf_token() ?>',
            },
            success:function(data){
                console.log(data);
                table.rows().remove().draw();
                for (var i = 0; i != data.length; i++){
                    if(data[i].status == "Pending"){
                        count++;
                    }
                }
                if(count != 0){
                    var approve = document.getElementById('approve_btn');
                    approve.classList.remove('d-none');
                }else{
                    var approve = document.getElementById('approve_btn');
                    approve.classList.add('d-none');
                }
                for (var i = 0; i != data.length; i++){
                    table.row.add([
                        data[i].status,
                        data[i].inclusive_date,
                        data[i].billed_to,
                        data[i].amount,
                    ]).draw(false);
                }
            }            
        });
    }
    

</script>
@endsection

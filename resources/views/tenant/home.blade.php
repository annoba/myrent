@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="offset-lg-1  mt-5">
           <h1 class="title-tag">Recent Announcements</h1>
        </div>
    </div>
    @php
    if(count($data)>0){
        $rec_date=$data->first()->created_at->format('M-d-y');
        $counter=0;
    }
    @endphp
    @if(count($data)>0)
        @foreach($data as $cards)
        @if($counter%4==0 || $counter==0)
        <div class="row mt-5">
                <div class="offset-lg-2"></div>
        @endif
            <div class="col-lg-2">
                <div class="card text-center">
                    @if($cards->level_of_urgency=='Casual')
                        <div class="card-header color-green text-left">
                    @else
                        <div class="card-header color-red text-left">
                    @endif
                    @if($cards->created_at->format('M-d-y')==$rec_date)
                        <strong>NEW!</strong>
                    @endif
                        </div>
                    <div class="card-body">
                        <h5 class="card-title"><strong>{{$cards->title}}</strong></h5>
                        <p class="card-text">{{$cards->body}}.</p>
                    </div>
                    <div class="card-footer text-muted">
                    {{$cards->created_at->format('M-d-y')}}
                    </div>
                </div>
            </div>
            @php
            $counter++;
            @endphp
        @if($counter%4==0)
            <div class="offset-lg-2"></div>
            </div>
        @endif
            @endforeach
        @else
        <div class="offset-lg-1">
            <h5><em><small class="text-muted"> No Recent Posts</small></em></h5>
        </div>
        @endif
    @if(count($data)>=4)
    <div class="row mt-3">
        <div class="offset-lg-9">
            <a href="/see-more/{{count($data)}}">See More</a>
        </div>
    </div>
    @endif
</div>
@endsection

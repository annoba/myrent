@extends('layouts.app')

@section('content')
<div class="container mt-3">
    <div class="row mt-5">
        {{-- Left Bar --}}
        <div class="col-lg-12">
            {{-- message panel --}}
            <div class="row" id="message-panel">
                <div class="col-lg-12" id="messages-container">
                @foreach($rooms as $message)
                    @if($message->sender=='tenant')
                        {{-- sent --}}
                        <div class="row" >
                            <div class="offset-lg-5 col-lg-7 msg text-right">
                                <p class="msg-sent">{{$message->message}}</p>
                            </div>
                        </div>
                    @elseif($message->sender=='landlord')
                        {{-- receive --}}
                        <div class="row">
                            <div class="offset-lg-1 col-lg-7 msg text-left">
                                <p class="msg-receive">{{$message->message}}</p>
                            </div>
                        </div>
                    @endif
                @endforeach                    
                </div>
            </div>
            {{-- submit panel --}}
            <div class="row" id="submit-panel">
                <div class="col-lg-12">
                    <form id="sendForm">
                        <div class="row form-group">
                            <div class="col-lg-10 no-pright">
                                <input type="text" id="msg-data" class="msg-height form-control"  placeholder="">
                            </div>
                            <div class="col-lg-2 no-pleft">
                                <button type="submit" id="send-msg" class="btn btn-custom btn-send">Send</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> 
    </div> 
</div>

<script>
updateScroll();
function updateScroll(){
var element = document.getElementById("message-panel");
element.scrollTop = element.scrollHeight;
}

$(function(){

    $('#sendForm').on('submit',function(e){
        e.preventDefault();
        var rID="{{auth()->user()->tenant_profile->room_id}}"
        var tID="{{auth()->user()->id}}";
        var msg=$('#msg-data').val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:"/send-message",
            method:'POST',
            data:{rID:rID,tID:tID,msg:msg,sender:"tenant"},
            success:function(data){
                if(data=="success"){
                    $('#messages-container').append('<div class="row" ><div class="offset-lg-5 col-lg-7 msg text-right"><p class="msg-sent">'+msg+'</p></div></div>');
                    updateScroll();
                    $('#msg-data').val('');
                    
                }
                // alert('asd');
            }, error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.responseText);
            }
        });
    });

    window.setInterval(function(){
        checkUpdate();
    }, 2000);

    // document.cookie.replace(/(?:(?:^|.*;\s*)test2\s*\=\s*([^;]*).*$)|^.*$/, "$1")
    var msg_index=-1;
    function checkUpdate(){
        var rid="{{auth()->user()->id}}";
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:"/recieve-msg",
            method:'POST',
            data:{id:rid},
            success:function(data){
                if(data.length>0){
                    if(msg_index==-1){
                        msg_index=data[data.length-1].id;
                    }else{
                        c_index=data[data.length-1].id;
                        if(c_index>msg_index){
                            $('#messages-container').append('<div class="row"><div class="offset-lg-1 col-lg-7 msg text-left"><p class="msg-receive">'+data[data.length-1].message+'</p></div></div>');   
                            msg_index=c_index;
                            updateScroll();
                        }
                    }
                }   
            }, error: function (xhr, ajaxOptions, thrownError) {
                // alert(xhr.responseText);
             }
        });
    }

});
</script>
@endsection

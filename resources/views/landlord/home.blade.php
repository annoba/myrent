@extends('layouts.app')

@section('content')
<div class="container mt-3 pb-5">
    <div class="row">
        <div class="col-sm-12">
            <h3 class="title-tag"> Announcements</h3>
        </div>
        <div class="col-sm-12 row">
            <div class="col-sm-8">
                <h5> Make an announcement for your tenants. </h5>
                <div class="col-sm-12">
                    <form method="post" action="{{url('/postAnnouncement')}}">
                    @csrf
                    <div class="row my-2">
                        <div class="my-2 col-5">
                            <label for="announcement_important">Level of Importance:</label>
                            <select class="form-control" name="level_of_urgency" id="gender">
                                <option value="Casual">Casual</option>
                                <option value="Important">Important</option>
                            </select>                    
                        </div>
                    </div> 
                    <div class="row my-2 col-12">
                        <label> Title:</label>
                        <input type="text" class="form-control" name="title" required>
                    </div>   
                    <div class="row my-2 col-12">
                        <label> Description:</label>
                        <textarea rows="8" class="form-control" name="body" required></textarea>
                    </div>
                    <button type="submit" class="btn btn-blue float-right mb-4" style="margin-right:30px;"> Post announcement </button> 
                    </form>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="col-sm-12">
                    <h5> Recent announcement </h5>
                </div>
                <div class="col-sm-12">
                    @if(count($announcements) == 0)
                    <div class="row">
                        <div class="col-sm-12">
                            <i class="mt-0" style="font-size: 9pt;"> No announcement posted yet</i>
                        </div>
                    </div> 
                    @else
                    @foreach($announcements as $announcement)
                    <div class="row">
                        <div class="col-sm-12">
                            <h4 class="mb-0"> {{ $announcement->title }} </h4>
                            <i class="mt-0" style="font-size: 8pt;"> Posted at {{$announcement->created_at}} </i>
                        </div>
                        <div class="col-sm-12">
                            <h5> {{$announcement->body}} </h5>
                        </div>                        
                    </div>  

                    @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('page_scripts')
<script>

// $(document).ready(function() {
//     $('#add-room-btn').click(function() {
//         var add_count = $("#room_count");
//     });
// });


</script>
@endsection
@extends('layouts.app')

@section('content')
<div class="container mt-3 pb-5">
    <div class="row">
        <div class="col-sm-12 row">
            <h3 class="font-weight-bold mb-0"> Add Rooms <h3>
            <button class="btn btn-link" type="submit" onClick="addClone()"> Add more </button>
        </div>
        <form action="{{ url('/addrooms') }}" method="post" class="col-sm-12">
        @csrf
        <input type="hidden" class="form-control" name="room_count" id="room_count" value="1">
            <div id="clone_here">
                <div class="my-3 col-sm-12" id="section-div-1">
                    <h4 class="font-weight-bold mt-2" id="section-number">Room # 1</h4>
                    <div class="row">
                        <div class="form-group mb-1 section-title col-sm-3">
                            <label>Room Name</label>
                            <input type="text" required class="form-control" name="room_name[]" >
                        </div>
                        <div class="form-group mb-1 section-description col-sm-3">
                            <label>Max # of Tenants</label>
                            <input type="number" class="form-control" name="room_maxtenants[]" min="0" value="1">
                        </div>
                        <div class="form-group mb-1 col-sm-3">
                            <label for="is_premium">Is it Premium?</label>
                            <select class="form-control" name="is_premium[]" id="is_premium">
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                            </select>
                        </div>
                    </div>
                    <!-- <div class="row">
                        <
                    </div> -->
                </div>
            </div>

            <div class="text-right" id="submit-section-btn">
                <button class="btn btn-blue" type="submit">Add rooms <i class="fa fa-angle-right"></i></button>
            </div>
        </form>
    </div>
</div>

@endsection

@section('page_scripts')
<script>

    var i = 1;
    function addClone(){
        i++;
        console.log(i);
        previous = document.getElementById('section-div-'+(i-1));
        clone_section = document.getElementById('section-div-1').cloneNode(true);
        clone_section.firstChild.nextElementSibling.innerHTML = "Room #" + i;
        clone_section.classList.add('section-div');
        clone_section.id = "section-div-"+i;
        document.getElementById('clone_here').appendChild(clone_section);
        document.getElementById('room_count').value = i;
    }

$(document).ready(function() {

    $('#add-room-btn').click(function() {
        old_val = 1;
        var add_count = $("#room_count");
        for(var i = old_val; i <= parseInt(add_count.val()); i++) {
            if(document.getElementById('section-div-'+i) != null) {
                if(document.getElementById('section-div-'+(i+1)) == null) {
                    // get clone-section div
                    clone_section = document.getElementById('clone-section').cloneNode(true);
                    console.log(clone_section.firstChild.innerHTML);
                    clone_section.firstChild.nextElementSibling.innerHTML = "Room #" + (i+1);
                    clone_section.classList.remove('d-none');
                    clone_section.classList.add('section-div');
                    clone_section.id = "section-div-"+(i+1);
                    $("#section-div-" + i).after(clone_section);
                }            
            ++old_val;
            ++i;                
            continue;
            }
            clone_section = document.getElementById('clone-section').cloneNode(true);
            clone_section.firstChild.nextElementSibling.innerHTML = "Room #" + i;
            clone_section.classList.remove('d-none');
            clone_section.classList.add('section-div');
            clone_section.id = "section-div-"+i;
            // console.log(clone_section.firstChild.nextElementSibling.innerHTML);
            if(old_val == 1) {
                if(i == 1) {
                    $("#section-count-input").after(clone_section);
                }
            } else {
                $("#section-div-" + (i-1)).after(clone_section);
            }
            old_val ++;
        }

        if(old_val == 1) {
            $("#add-section-btn").removeClass('btn-outline-success');
            $("#add-section-btn").addClass("btn-outline-primary");
            if($("#current-count").val() == 0) {
                $("#submit-section-btn").addClass('d-none');
            }
        }else{
            $("#add-section-btn").removeClass('btn-outline-primary');
            $("#add-section-btn").addClass("btn-outline-success");
            $("#submit-section-btn").removeClass('d-none');
        }
    });
});


</script>
@endsection
@extends('layouts.app')

@section('content')
<div class="container mt-3 pb-5">
    <div class="row">
    <div class="col-sm-12">
            <h3 class="font-weight-bold mb-0"> Bill Types <h3>
        </div>
        <div class="col-sm-12 row">
            <div class="col-sm-4">
                <form method="post" action="{{url('/billtypes')}}">
                @csrf
                        <label> Bill Type  Name:</label>
                        <input type="text" class="form-control" name="bill_type" required>
                    
                        <label> Bill Type  Description:</label>
                        <input type="text" class="form-control" name="bill_description" required>
                
                        <button type="submit" class="btn btn-blue float-right mt-3"> Add bill type </button> 
                </form>
            </div>
            <div class="col-sm-8">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Bill Names</th>
                        <th>Bill Description</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @if(count($bill_types) == 0)
                        <tr>
                            <td>No bill added yet</td>
                            <td>-</td>
                            <td>-</td>
                        </tr>
                        @else
                        @foreach($bill_types as $bill_type)
                        <tr>
                            <td>{{ $bill_type->type }}</td>
                            <td>{{ $bill_type->description }}</td>
                            <td><i class="fa fa-remove"></i></td>
                        </tr>
                        @endforeach
                        @endif

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

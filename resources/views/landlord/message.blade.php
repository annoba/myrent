@extends('layouts.app')

@section('content')
<div class="container mt-3 pb-5">
    <div class="row mt-5">
        {{-- Right Bar --}}
        <div class="col-lg-4" id="side-message">
            {{-- SEARCH --}}
            <div class="row form-group">
                <label for="search" class="col-lg-3 col-form-label text-md-right"><em>{{ __('Search') }}</em></label>
                <input type="text" name="search" class="col-lg-8 center-obj input-custom-md form-control"  placeholder="" required>
                
            </div>
            {{-- Body --}}
            @foreach($rooms['tenants'] as $room)
                @if($rooms['id']==$room->user_id)
                <div class="row user-info user-info-active" data-room="{{$room->room_id}}" id="{{$room->user_id}}"> 
                @else
                <div class="row user-info" data-room="{{$room->room_id}}" id="{{$room->user_id}}"> 
                @endif
                    <div class="col-lg-4">
                        <img src="http://placehold.it/200x200" class="user-img" alt="profile_pic">
                    </div>
                    <div class="col-lg-8">
                    <h6 class="mt-2"><strong>{{$room->first_name}} {{$room->last_name}}</strong></h6>
                    </div>          
                </div>
            @endforeach

        </div>
        {{-- Left Bar --}}
        <div class="col-lg-8">
            {{-- message panel --}}
            <div class="row" id="message-panel">
                <div class="col-lg-12" id="messages-container">
                @foreach($rooms['message'] as $message)
                    @if($message->sender=='landlord')
                        {{-- sent --}}
                        <div class="row" >
                            <div class="offset-lg-5 col-lg-7 msg text-right">
                                <p class="msg-sent">{{$message->message}}</p>
                            </div>
                        </div>
                    @elseif($message->sender=='tenant')
                        {{-- receive --}}
                        <div class="row">
                            <div class="offset-lg-1 col-lg-7 msg text-left">
                                <p class="msg-receive">{{$message->message}}</p>
                            </div>
                        </div>
                    @endif
                @endforeach                    
                </div>
            </div>
            {{-- submit panel --}}
            <div class="row" id="submit-panel">
                <div class="col-lg-12">
                    <form id="sendForm">
                        <div class="row form-group">
                            <div class="col-lg-10 no-pright">
                                <input type="text" id="msg-data" class="msg-height form-control"  placeholder="">
                            </div>
                            <div class="col-lg-2 no-pleft">
                                <button type="submit" id="send-msg" class="btn btn-custom btn-send">Send</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> 
    </div> 
</div>

<script>
updateScroll();
function updateScroll(){
var element = document.getElementById("message-panel");
element.scrollTop = element.scrollHeight;
}

$(function(){

    $('#sendForm').on('submit',function(e){
        e.preventDefault();

        var rID=$('.user-info-active').data('room');
        var tID=document.querySelector('.user-info-active').id;
        var msg=$('#msg-data').val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:"/send-message",
            method:'POST',
            data:{rID:rID,tID:tID,msg:msg,sender:"landlord"},
            success:function(data){
                if(data=="success"){
                    $('#messages-container').append('<div class="row" ><div class="offset-lg-5 col-lg-7 msg text-right"><p class="msg-sent">'+msg+'</p></div></div>');
                    updateScroll();
                    $('#msg-data').val('');
                    
                }
                // alert('asd');
            }, error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.responseText);
            }
        });
    });

    window.setInterval(function(){
        checkUpdate();
    }, 2000);

    // document.cookie.replace(/(?:(?:^|.*;\s*)test2\s*\=\s*([^;]*).*$)|^.*$/, "$1")
    var msg_index=-1;
    function checkUpdate(){
        var rid=document.querySelector('.user-info-active').id;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:"/recieve-msg",
            method:'POST',
            data:{id:rid},
            success:function(data){
                if(data.length>0){
                    if(msg_index==-1){
                        msg_index=data[data.length-1].id;
                    }else{
                        c_index=data[data.length-1].id;
                        if(c_index>msg_index){
                            $('#messages-container').append('<div class="row"><div class="offset-lg-1 col-lg-7 msg text-left"><p class="msg-receive">'+data[data.length-1].message+'</p></div></div>');   
                            msg_index=c_index;
                            updateScroll();
                        }
                    }
                }   
            }, error: function (xhr, ajaxOptions, thrownError) {
                // alert(xhr.responseText);
             }
        });
    }

    $('.user-info').on('click',function(){
        $id=this.id;
        window.location.href='/message/landlord/'+$id;
        $rem_id=document.querySelector('.user-info-active').id;
        $('#'+$rem_id).removeClass('user-info-active');
        $(this).addClass('user-info-active');
    });
});
</script>
@endsection

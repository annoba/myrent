@extends('layouts.app')

@section('content')
<div class="container mt-3 pb-5">
    <div class="row">
        <div class="col-sm-12 row">
            <h3 class="font-weight-bold mb-0"> Add Tenants <h3>
            <button class="btn btn-link" type="submit" onClick="addClone()"> Add more </button>
        </div>
        <form action="{{ url('/addtenants') }}" method="post" class="col-sm-12">
        @csrf
        <input type="hidden" class="form-control" name="tenant_count" id="tenant_count" value="1">
        <div id="clone_here">
            <div class="my-3 col-sm-12" id="section-div-1">
                <h4 class="font-weight-bold mt-2" id="section-number">Tenant # 1</h4>
                <div class="col-sm-12 row" style="width:94.5%">
                    <h5 class="font-weight-bold"> Personal Information </h5>
                        <div class="col-sm-12 row ">
                            <div class="form-group mb-1 section-title col-sm-6">
                                <label>Last Name</label>
                                <input type="text" required class="form-control" name="last_name[]" >
                            </div>
                            <div class="form-group mb-1 section-description  col-sm-6">
                                <label>First Name</label>
                                <input type="text" required class="form-control" name="first_name[]">
                            </div>
                        </div>
                        <div class="col-sm-12 row">
                            <div class="form-group mb-1 section-title col-sm-4">
                                <label>Contact Number</label>
                                <input type="text" required class="form-control" name="contact_number[]" >
                            </div>
                            <div class="form-group mb-1 col-sm-4">
                                <label for="gender">Gender</label>
                                <select class="form-control" name="gender[]" id="gender">
                                    <option value="Female">Female</option>
                                    <option value="Male">Male</option>
                                </select>
                            </div>
                            <div class="form-group mb-1 section-title col-sm-4">
                                <label>Birth date</label>
                                <input type="date" class="form-control" name="birth_date[]" >
                            </div>
                        </div>
                </div>
                <div class="col-sm-12 row">
                    <div class="col-sm-6 row">
                        <h5 class="font-weight-bold mt-3"> Account Information </h5>
                            <div class="col-sm-12 row">
                                <div class="form-group mb-1 section-title col-sm-6">
                                    <label>Username</label>
                                    <input type="text" class="form-control" name="username[]" >
                                </div>
                                <div class="form-group mb-1 section-title col-sm-6">
                                    <label>Email</label>
                                    <input type="text" class="form-control" name="email[]" >
                                </div>
                            </div>
                            <div class="row col-sm-12">
                                <div class="form-group mb-1 section-title col-sm-6">
                                    <label>Password</label>
                                    <input type="password" class="form-control" name="password[]" id="password-1">
                                </div>
                                <div class="form-group mb-1 section-title col-sm-6">
                                    <label>Confirm Password</label>
                                    <input type="password" class="form-control" name="confirm_password[]" id="confirm_password-1">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 row">
                            <h5 class="font-weight-bold mt-3 col-sm-12"> Stay Information </h5>
                            <div class="row col-sm-12">
                                <div class="form-group section-title mb-1 col-sm-12">
                                    <label>Room Name</label>
                                    <input type="text" class="form-control" id="room_name-1" name="room_id[]" autocomplete="off" required>
                                    <div class="form-group" id="room_list-1" style="z-index: 10;position:absolute;"></div>
                                </div>
                            </div>
                            <div class="col-sm-12 row">
                                <div class="form-group mb-1 section-title col-sm-6">
                                    <label>Start date</label>
                                    <input type="date" class="form-control" name="start_date[]" >
                                </div>
                                <div class="form-group mb-1 section-title col-sm-6">
                                    <label>End date</label>
                                    <input type="date" class="form-control" name="end_date[]" >
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-right" id="submit-tenant-btn">
                <button class="btn btn-blue" type="submit">Add Tenant</button>
        </div>
        </form>
    </div>
</div>

@endsection

@section('page_scripts')
<script>

    

    var i = 1;
    function addClone(){
        i++;
        console.log(i);
        previous = document.getElementById('section-div-'+(i-1));
        clone_section = document.getElementById('section-div-1').cloneNode(true);
        clone_section.firstChild.nextSibling.nextSibling.nextSibling.nextSibling.nextSibling.childNodes[1].childNodes[5].childNodes[1].childNodes[3].id = "password-"+i;
        clone_section.firstChild.nextSibling.nextSibling.nextSibling.nextSibling.nextSibling.childNodes[1].childNodes[5].childNodes[3].childNodes[3].id = "confirm_password-"+i;
        clone_section.firstChild.nextSibling.nextSibling.nextSibling.nextSibling.nextSibling.childNodes[3].childNodes[3].childNodes[1].childNodes[3].id = "room_name-"+i;
        clone_section.firstChild.nextSibling.nextSibling.nextSibling.nextSibling.nextSibling.childNodes[3].childNodes[3].childNodes[1].childNodes[5].id = "room_list-"+i;
        clone_section.firstChild.nextElementSibling.innerHTML = "Tenant #" + i;
        clone_section.classList.add('section-div');
        clone_section.id = "section-div-"+i;
        document.getElementById('clone_here').appendChild(clone_section);
        document.getElementById('tenant_count').value = i;
        addRoomNameActivity(i);
    }

    function addRoomNameActivity(i){
        $('#room_name-'+i).keyup(function(){  
            var room_name= $(this);
            var output = "";
            console.log(room_name);
            var query = $(this).val();
            if(query != '')
            {
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url:"{{ route('landlord.getRooms') }}",
                    method:"POST",
                    data:{query:query, _token:_token},
                    success:function(data){
                        console.log(data);
                        $('#room_list-'+i).fadeIn(); 
                        output += '<div class="list-group" style="display:block; position:relative; width:120px;">';
                        for(var j = 0; j != data.length; j++){
                            output += '<a class="list-group-item room_list-'+i+'" href="#">'+data[j].room_number+'</a>';
                        }
                        output += '</div>';
                        console.log(output);
                        $('#room_list-'+i).html(output);
                        console.log($('#room_list-'+i));
                        clone_section = document.getElementById('section-div-1').cloneNode(true);
                    }
                });    
            }
        });

        $(document).on('click', '.room_list-'+i, function(){  
            $('#room_name-'+i).val($(this).text());  
            $('#room_list-'+i).fadeOut();  
        });

        $('#confirm_password-'+i).keyup(function(){
            var confirm = $(this).val();
            if ($('#password-1').val() === confirm){
                console.log('yes');
            }else{
                console.log('no');
            }
        });
    }

    $('#room_name-1').keyup(function(){  
        var room_name= $(this);
        var output = "";
        console.log(room_name);
        var query = $(this).val();
        if(query != '')
        {
            var _token = $('input[name="_token"]').val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url:"{{ route('landlord.getRooms') }}",
                method:"POST",
                data:{query:query, _token:_token},
                success:function(data){
                    console.log(data);
                    $('#room_list-1').fadeIn(); 
                    output += '<div class="list-group" style="display:block; position:relative; width:120px;">';
                        for(var j = 0; j != data.length; j++){
                            output += '<a class="list-group-item room_list-1" href="#">'+data[j].room_number+'</a>';
                        }
                        output += '</div>';
                        console.log(output);
                    $('#room_list-1').html(output);
                }
            });    
        }
    });

    $(document).on('click', '.room_list-1', function(){  
        $('#room_name-1').val($(this).text());  
        $('#room_list-1').fadeOut();  
    });

    $('#confirm_password-1').keyup(function(){
            var confirm = $(this).val();
            // console.log(confirm);
            // console.log($('#password-1').val());
            if ($('#password-1').val() === confirm){
                console.log('yes');
            }else{
                console.log('no');
            }
        });

</script>
@endsection
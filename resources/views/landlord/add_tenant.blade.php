@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h3 class="font-weight-bold mb-0"> Add Tenants <h3>
        </div>
        <?php $old = isset(Session::getOldInput()['tenant_count']) ? Session::getOldInput()['tenant_count'] : 0 ?>
        <form action="{{ url('/addtenants') }}" method="post" class="col-sm-12">
        @csrf
            <div class="col-sm-6" id="section-count-input">
                <div class="input-group">
                    <input type="number" class="form-control" name="tenant_count" id="tenant_count" placeholder="Current number of tenants: {{count($tenant_data)}}">
                    <div class="input-group-append">
                        <button class="btn btn-outline-primary" id="add-tenant-btn" type="button"><i class="fa fa-plus mr-2"></i>Add</button>
                    </div>
                </div>
            </div>

            <div class="my-3 col-sm-12 d-none" id="clone-section">
                <h4 class="font-weight-bold mt-2" id="section-number">Tenant #</h4>
                <h5 class="font-weight-bold"> Personal Information </h5>
                <div class="row">
                    <div class="form-group mb-1 section-title col-sm-3">
                        <label>Last Name</label>
                        <input type="text" class="form-control" name="last_name[]" >
                    </div>
                    <div class="form-group mb-1 section-description col-sm-3">
                        <label>First Name</label>
                        <input type="text" class="form-control" name="first_name[]">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group mb-1 section-title col-sm-3">
                        <label>Contact Number</label>
                        <input type="text" class="form-control" name="contact_number[]" >
                    </div>
                    <div class="form-group mb-1 col-sm-3">
                        <label for="gender">Gender</label>
                        <select class="form-control" name="gender[]" id="gender">
                            <option value="Female">Female</option>
                            <option value="Male">Male</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group mb-1 section-title col-sm-3">
                        <label>Birth date</label>
                        <input type="date" class="form-control" name="birth_date[]" >
                    </div>
                </div>
                <h5 class="font-weight-bold mt-3"> Stay Information </h5>
                <div class="row">
                    <div class="form-group mb-1 section-title col-sm-3">
                        <label>Room Name</label>
                        <input type="text" class="form-control" id="room_name" name="room_id[]" autocomplete="off" required>
                        <div class="form-group" id="room_list" style="z-index: 10;position:absolute;"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group mb-1 section-title col-sm-3">
                        <label>Start date</label>
                        <input type="date" class="form-control" name="start_date[]" >
                    </div>
                    <div class="form-group mb-1 section-title col-sm-3">
                        <label>End date</label>
                        <input type="date" class="form-control" name="end_date[]" >
                    </div>
                </div>

                <h5 class="font-weight-bold mt-3"> Account Information </h5>
                <div class="row">
                    <div class="form-group mb-1 section-title col-sm-3">
                        <label>Username</label>
                        <input type="text" class="form-control" name="username[]" >
                    </div>
                </div>
                <div class="row">
                    <div class="form-group mb-1 section-title col-sm-3">
                        <label>Email</label>
                        <input type="text" class="form-control" name="email[]" >
                    </div>
                    <div class="form-group mb-1 section-title col-sm-3">
                        <label>Password</label>
                        <input type="password" class="form-control" name="password[]" >
                    </div>
                </div>
            </div>
            <div class="text-right d-none" id="submit-tenant-btn">
                <button class="btn btn-primary" type="submit">Add Tenant <i class="fa fa-angle-right"></i></button>
            </div>
        </form>
    </div>
</div>

@endsection

@section('page_scripts')
<script>
$(document).ready(function() {

    var add_count = 0;
    var room_names ="";

    $(document).on('click', 'a', function(){  
        $('#room_name').val($(this).text());  
        $('#room_list').fadeOut();  
    });

    $('#add-tenant-btn').click(function() {
        old_val = 1;
        add_count = $("#tenant_count");
        for(var i = old_val; i <= parseInt(add_count.val()); i++) {
            if(document.getElementById('section-div-'+i) != null) {
                if(document.getElementById('section-div-'+(i+1)) == null) {
                    // get clone-section div
                    clone_section = document.getElementById('clone-section').cloneNode(true);
                    clone_section.firstChild.nextElementSibling.innerHTML = "Tenant #" + (i+1);
                    clone_section.classList.remove('d-none');
                    clone_section.classList.add('section-div');
                    clone_section.id = "section-div-"+(i+1);
                    $("#section-div-" + i).after(clone_section);
                    document.getElementById('room_name').classList.add("room_name_"+(i+1));
                    if(i == add_count){
                        room_names += ".room_name_"+i+1;
                    }else{
                        room_names += ".room_name_"+(i+1)+",";
                    }
                    // console.log(room_names);
                }
            ++old_val;
            ++i;                
            continue;
            }
            clone_section = document.getElementById('clone-section').cloneNode(true);
            clone_section.firstChild.nextElementSibling.innerHTML = "Tenant #" + i;
            clone_section.classList.remove('d-none');
            clone_section.classList.add('section-div');
            clone_section.id = "section-div-"+i;
            document.getElementById('room_name').classList.add("room_name_"+i);
            if(i === add_count.val()){
                room_names += ".room_name_"+i;
            }else{
                room_names += ".room_name_"+i+",";
            }
            console.log(room_names);
            if(old_val == 1) {
                if(i == 1) {
                    $("#section-count-input").after(clone_section);
                }
            } else {
                $("#section-div-" + (i-1)).after(clone_section);
            }
            old_val ++;
        }

        if(old_val == 1) {
            $("#add-tenant-btn").removeClass('btn-outline-success');
            $("#add-tenant-btn").addClass("btn-outline-primary");
            if($("#current-count").val() == 0) {
                $("#submit-tenant-btn").addClass('d-none');
            }
        }else{
            $("#add-tenant-btn").removeClass('btn-outline-primary');
            $("#add-tenant-btn").addClass("btn-outline-success");
            $("#submit-tenant-btn").removeClass('d-none');
        }
        $(room_names).keyup(function(){  
        var room_name= $(this);
        console.log(room_name);
        var id = room_name.substring((room_name.length - 1), room_name.length);
        var query = $(this).val();
        if(query != '')
        {
            var _token = $('input[name="_token"]').val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url:"{{ route('landlord.getRooms') }}",
                method:"POST",
                data:{query:query, _token:_token},
                success:function(data){
                    console.log(data);
                    $('#room_list').fadeIn();  
                    $('#room_list').html(data);
                }
            });    
        }
    });
    });

    
    
});



</script>
@endsection
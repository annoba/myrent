@extends('layouts.app')

@section('content')
<div class="container-fluid mt-3 pb-5">
    <div class="row">
        <div class="offset-lg-2  mt-5">
           <h1 class="title-tag">Profile Account</h1>
        </div>
    </div>
    <div class="row mt-5">
        <div class="offset-lg-2 col-lg-8">
            <form action="{{route('edit.prof.landlord')}}" method="POST"> 
                    @csrf
            <div class="row">
                <div class="col-lg-3">
                    <img src="http://ei.is.tuebingen.mpg.de/assets/noEmployeeImage_md-eaa7c21cc21b1943d77e51ab00a5ebe9.png" class="prof-photo" alt="prof image" height="250" width="250">
                </div>
                <div class="offset-lg-1 col-lg-4">
                    <div class="row">
                        <div class="col-lg-12">
                        <label for="Firstname">First Name</label>
                        <input type="text" value="{{$data->first()->first_name}}" name="Firstname" class="center-obj form-control"  placeholder="Enter first name" required>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-lg-12">
                        <label for="Lastname">Last Name</label>
                            <input type="text" value="{{$data->first()->last_name}}" name="Lastname" class="center-obj form-control"  placeholder="Enter last name" required>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-lg-12">
                        <label for="contnum">Contact Number</label>
                            <input type="text" maxlength="11" value="{{$data->first()->contact_number}}" name="contnum" class="form-control number"  placeholder="Enter contact number" required>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div id="radioBtn" class="btn-group col-lg-6">
                        <label for="gender" class="mr-3">Gender</label>
                        @if($data->first()->gender=='Male')
                            <a class="btn btn-primary btn-sm active"  data-toggle="happy" data-title="Male">Male</a>
                            <a class="btn btn-primary btn-sm notActive" data-toggle="happy" data-title="Female">Female</a>
                            <input type="hidden" id='gender' value='Male' name='gender'>
                        @else
                            <a class="btn btn-primary btn-sm notActive"  data-toggle="happy" data-title="Male">Male</a>
                            <a class="btn btn-primary btn-sm active" data-toggle="happy" data-title="Female">Female</a>
                            <input type="hidden" id='gender' value='Female' name='gender'>
                        @endif
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="row">
                        <div class="col-lg-12">
                            <label for="email">Email</label>
                            <input type="mail" value="{{$data->first()->email}}" name="mail" class="form-control"  placeholder="Enter email" required>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-lg-12">
                            <label for="username">Username</label>
                            <input type="text" value="{{$data->first()->username}}" name="username" class="form-control"  placeholder="Enter username" required>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-lg-12">
                            <label for="bdate">Birthdate</label>
                        <input type="date" class="form-control" value="{{$data->first()->birthday}}" name="bdate">
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-lg-12 mt-4">
                            <button type="submit" class="btn btn-primary btn-custom float-right">Update</button>
                        </div>
                    </div>
                </div>
                
            </div>
            </form>
        </div>
    </div>
</div>
<script>
$(function(){
    $('.number').keydown(function(e){
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    $('#radioBtn a').on('click', function(){
        var sel = $(this).data('title');
        var tog = $(this).data('toggle');
        $('#gender').prop('value', sel);

        $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
        $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
    });
});
</script>
@endsection

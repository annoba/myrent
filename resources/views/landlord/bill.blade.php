@extends('layouts.app')

@section('content')
<div class="container mt-3 pb-5">
    <div class="row">
        <div class="col-sm-12 row" style="margin:0px;">
            <h3 class="font-weight-bold mb-0"> Bills <h3> &nbsp
            <h6 style="margin-top:10px;"> <a href="{{route('landlord.typebill')}}"> Modify</a> </h6>
        </div>
        <div class="col-sm-12">
            <h5> Manage your tenant's bills </h5>    
        </div>
        <form method="post" class="col-sm-12" action="{{route('landlord.postbill')}}">
            @csrf
            
            <div class="col-sm-12 row" id="once_freq">
                <div class="col-sm-6">
                    <label class="mt-2"> Bill Start Date:</label>
                    <input type="date" class="form-control" name="start_date" min="1" required>
                </div>
                <div class="col-sm-6">
                    <label class="mt-2"> Bill End Date:</label>
                    <input type="date" class="form-control" name="end_date" min="1" required>
                </div>
            </div>
            <div class="col-sm-12 row d-none" id="monthly_freq">
                <div class="col-sm-6">
                    <label class="mt-2"> Every:</label>
                    <select class="form-control" name="type_of_bill" id="type_of_bill" required>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                        <option value="21">21</option>
                        <option value="22">22</option>
                        <option value="23">23</option>
                        <option value="24">24</option>
                        <option value="25">25</option>
                        <option value="26">26</option>
                        <option value="27">27</option>
                        <option value="28">28</option>
                    </select>                 
                </div>
            </div>
            <div class="col-sm-12 row mr-0">
                <div class="col-sm-6">
                    <label for="type_of_bill" class="mt-2">Type of Bill:</label>
                    <select class="form-control" name="type_of_bill" id="type_of_bill" required>
                        @if(count($data['bill_types']) == 0)
                        <option value="0">No bills added yet</option>
                        @else
                            @foreach($data['bill_types'] as $bill_type)
                            <option value="{{$bill_type->id}}">{{$bill_type->type}}</option>
                            @endforeach
                        @endif
                    </select> 
                </div>
                <div class="col-sm-6 tb">
                    <label class="mt-2"> Room Name:</label>
                    <input type="text" class="form-control" id="room_name" name="room_name" autocomplete="off" required> 
                    <div id="room_list" style="z-index: 10;position:absolute;"></div>
                </div>
            </div>
            
            <div class="col-sm-12 row">
                <div class="col-sm-6">
                    <label class="mt-2"> Amount:</label>
                    <input type="number" class="form-control" name="amount" min="1" required>
                </div>
                <div class="col-sm-6">
                    <label class="mt-2"> Message:</label><br>
                    <textarea class="form-control" required name="bill_message" rows="3" cols="60"> </textarea>   
                    <button type="submit" class="btn btn-blue float-right mt-3"> Post bill</button> 
                </div>
            </div>
        </form>
    </div>
</div>

@endsection

@section('page_scripts')
<script>
$(document).ready(function(){


    $('.edit').click(function(){
        var query = $(this).attr('bill-id');
        console.log(query);
        if(query != '')
        {
            // var _token = $('input[name="_token"]').val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url:"{{ route('landlord.getBillData') }}",
                method:"POST",
                data:{
                    query:query, _token: '<?php echo csrf_token() ?>',
                    },
                success:function(data){
                    //$('#myModal').toggle('modal');
                    console.log(data);
                    $('#id_editmodal').val(data.id);
                    $('#room_name_editmodal').html(data.room_name);
                    $('#bill_type_editmodal').html(data.bill_type);
                    $('#amount_editmodal').val(data.amount);
                    $('#message_editmodal').val(data.message);
                }            
            });
        }
    });

    $('.check').click(function(){
        var query = $(this).attr('bill-id');
        console.log(query);
        if(query != '')
        {
            // var _token = $('input[name="_token"]').val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url:"{{ route('landlord.getBillData') }}",
                method:"POST",
                data:{
                    query:query, _token: '<?php echo csrf_token() ?>',
                    },
                success:function(data){
                    //$('#myModal').toggle('modal');
                    console.log(data);
                    $('#id_checkmodal').val(data.id);
                    $('#room_name_checkmodal').html("Room "+data.room_name);
                    $('#amount_checkmodal').html(data.amount);
                }            
            });
        }
    });

    $('#room_name').keyup(function(){
        var query = $(this).val();
        console.log(query);
        var output = "";
        if(query != '')
        {
            // var _token = $('input[name="_token"]').val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url:"{{ route('landlord.getRooms') }}",
                method:"POST",
                data:{
                    query:query, _token: '<?php echo csrf_token() ?>',
                    },
                success:function(data){
                    console.log(data);
                    $('#room_list').fadeIn();  
                    output += '<div class="list-group" style="display:block; position:relative; width:120px;">';
                        for(var j = 0; j != data.length; j++){
                            output += '<a class="list-group-item room_list-1" href="#">'+data[j].room_number+'</a>';
                        }
                    output += '</div>';
                    console.log(output);
                    $('#room_list').html(output);
                }            
            });
        }
    });

    $(document).on('click', 'a', function(){  
        $('#room_name').val($(this).text());  
        $('#room_list').fadeOut();  
    }); 



});

</script>
@endsection

@extends('layouts.app')

@section('content')
<div class="container mt-3 pb-5 pb-5">
    <div class="row">
        <div class="col-sm-12 row" style="margin:0px;">
            <h3 class="font-weight-bold mb-0"> Rooms <h3> &nbsp
        </div>
        <div class="col-sm-12 row">
            <div class="col-sm-9">
                <h5> Number of rooms: {{count($data['rooms'])}} </h5> 
            </div>
            <div class="col-sm-3 form-group">
                <select class="form-control " name="room_status" id="room_status">
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                </select>
            </div>
        </div>
        <div class="col-sm-12 mt-1">
            <table class="table table-hover" id="room_table">
                <thead>
                <tr>
                    <th>Room Name</th>
                    <th>isPremium</th>
                    <th>Max # Of Tenants</th>
                    <th>Current # of Tenants</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody id="tbody-rooms">
                    @if(count($data['rooms']) == 0)
                    <tr>
                        <td>No room added yet</td>
                        <td>0</td>
                        <td>0</td>
                        <td>-</td>
                    </tr>
                    @else
                    @foreach($data['rooms'] as $room)
                    <tr>
                        <td>{{ $room['room_name'] }}</td>
                        <td>@if($room['isPremium'] == 1)
                            Pending
                            @elseif($room['isPremium']==2)
                            No
                            @else
                            Yes
                            @endif
                        </td>
                        <td>{{ $room['max_tenants'] }}</td>
                        <td>{{ $room['curr_tenants'] }}</td>
                        <td>
                            <i class="fa fa-edit edit-room" room-id="{{$room['id']}}" style="font-size:19px" data-toggle="modal" data-target="#editRoomModal"></i>
                            <i class="fa fa-remove remove-room" style="font-size:19px" room-id="{{$room['id']}}" data-toggle="modal" data-target="#removeRoomModal"></i>
                        </td>
                    </tr>
                    @endforeach
                    @endif

                </tbody>
            </table>
        </div>
    </div>

    <div class="row mt-4">
        <div class="col-sm-12 row"  style="margin:0px;">
            <h3 class="font-weight-bold mb-0"> Tenants <h3> &nbsp
        </div>
        <div class="col-sm-12 row">
            <div class="col-sm-9">
                <h5> Number of tenants: {{count($data['tenants'])}} </h5> 
            </div>
            <div class="col-sm-3 form-group">
                <select class="form-control " name="tenant_status" id="tenant_status">
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                </select>
            </div>
        </div>
        <div class="col-sm-12 mt-1">
            <table class="table table-hover" id="tenants_table">
                <thead>
                    <tr>   
                        <th>Room Name</th>
                        <th>Last Name</th>
                        <th>First Name</th>
                        <th>Contact Number</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody id="tbody-tenant">
                    @if(count($data['tenants']) == 0)
                    <tr>
                        <td>No tenant is active</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                    @else
                    @foreach($data['tenants'] as $tenant)
                    <tr>
                        <td>{{ $tenant->room->room_number }}</td>
                        <td>{{ $tenant->last_name }}</td>
                        <td>{{ $tenant->first_name }}</td>
                        <td>{{ $tenant->contact_number }}</td>
                        <td>
                            <i class="fa fa-edit edit-tenant" tenant-id="{{$tenant->id}}" style="font-size:19px" data-toggle="modal" data-target="#editTenantModal"></i>
                            <i class="fa fa-remove remove-tenant" tenant-id="{{$tenant->id}}" style="font-size:19px" data-toggle="modal" data-target="#removeTenantModal"></i>
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>

    <div class="row mt-4">
        <div class="col-sm-12 row" style="margin:0px;">
            <h3 class="font-weight-bold"> Bills <h3>
        </div>
        <div class="col-sm-12 row">
            <div class="col-sm-9">
                <h5> Number of unpaid bills: {{count($data['bills'])}} </h5> 
            </div>
            <div class="col-sm-3 form-group">
                <select class="form-control " name="bills_status" id="bills_status">
                    <option value="Pending">Pending</option>
                    <option value="Paid">Paid</option>
                </select>
            </div>
        </div>
        <div class="col-sm-12 mt-1">
            <table class="table table-hover" id="bill_table">
            <thead>
                <tr>
                    <th>Inclusive Date</th>
                    <th>Room Name</th>
                    <th>Bill Type</th>
                    <th>Amount</th>
                    <th>Message</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody id="tbody-bills">
                @if(count($data['bills']) == 0)
                    <tr>
                        <td>No bill is pending</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                    @else
                    @foreach($data['bills'] as $bill)
                    <tr>
                        <td>{{$bill->start_date.' - '. $bill->end_date}}</td>
                        <td>{{$bill->room->room_number}}</td>
                        <td>{{$bill->billing_type['type']}}</td>
                        <td>{{$bill->amount}}</td>
                        <td>{{$bill->message}}</td>
                        <td>
                            <i class="fa fa-edit edit-bill" bill-id="{{$bill->id}}" style="font-size:19px" data-toggle="modal" data-target="#editBillModal"></i>
                            <i class="fa fa-check check-bill" bill-id="{{$bill->id}}" style="font-size:19px" data-toggle="modal" data-target="#checkBillModal"></i>
                        </td>
                    </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="editTenantModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                <h4 class="modal-title"> Edit Tenant Information </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form method="post" enctype="multipart/form-data" autocomplete="false" action="{{ url('/edittenant') }}">
                @csrf
                <input type="hidden" name="id" id="id_edittenantmodal" value="">
                    <div class="modal-body col-sm-12">
                        <div class="col-sm-12">
                            <b> Tenant Name: </b> <p id="tenant_name_edittenantmodal"> </p>
                        </div>
                        <div class="col-sm-12 row mt-2">
                            <div class="col-sm-6">
                                <b> Birthdate: </b>   <input type="date" class="form-control" name="birthdate_edittenantmodal" id="birthdate_edittenantmodal" readonly>
                            </div>  
                            <div class="col-sm-6">
                                <b> Gender: </b>   <p id="gender_edittenantmodal"> </p>
                            </div>                  
                        </div>
                        <div class="col-sm-12 mt-2 pr-2" style="width:92%">
                            <b> Contant Number: </b>  <input type="text" class="form-control" name="contactnumber_edittenantmodal" id="contactnumber_edittenantmodal">        
                        </div>
                        <div class="col-sm-12 row mt-2">
                            <div class="col-sm-6">
                                <b> Start Date: </b>   <input type="date" class="form-control" name="startdate_edittenantmodal" id="startdate_edittenantmodal">
                            </div>  
                            <div class="col-sm-6">
                                <b> End Date: </b>  <input type="date" class="form-control" name="enddate_edittenantmodal" id="enddate_edittenantmodal">
                            </div>                  
                        </div>
                    </div>
                
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" name="submit">Save</button>
                    </form>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="removeTenantModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                <h4 class="modal-title"> Tenant Removal </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form method="post" enctype="multipart/form-data" autocomplete="false" action="{{ url('/removetenant') }}">
                @csrf
                <input type="hidden" name="id" id="id_removetenantmodal" value="">
                <div class="modal-body col-sm-12">
                    Are you sure you want to remove <b id="tenant_removetenantmodal"></b>?
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" name="submit">Yes</button>
                    </form>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editRoomModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                <h4 class="modal-title"> Edit Room </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form method="post" enctype="multipart/form-data" autocomplete="false" action="{{ url('/editroom') }}">
                @csrf
                <input type="hidden" name="id" id="id_editroommodal" value="">
                    <div class="modal-body col-sm-12">
                        <div class="col-sm-12">
                            <b> Room Name: </b> <p id="room_name_editroommodal"> </p>
                        </div>
                        <div class="col-sm-12">
                            <b> Max $ of Tenants: </b> <input type="number" class="form-control" name="numtenants_editmodal" id="numtenants_editmodal">
                                                   
                        </div>
                        <div class="col-sm-12 mt-2">
                            <b> isPremium: </b> 
                            <select class="form-control" name="is_premium_editmodal" id="is_premium_editmodal">
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                            </select> 
                        </div>
                    </div>
                
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" name="submit">Save</button>
                    </form>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="removeRoomModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                <h4 class="modal-title"> Room Removal </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form method="post" enctype="multipart/form-data" autocomplete="false" action="{{ url('/removeroom') }}">
                @csrf
                <input type="hidden" name= "id" id="id_removeroommodal" value="">
                <div class="modal-body col-sm-12">
                    Are you sure you want to remove <b id="room_name_removeroommodal"></b>?
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" name="submit">Yes</button>
                    </form>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editBillModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                <h4 class="modal-title"> Edit Bill </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form method="post" enctype="multipart/form-data" autocomplete="false" action="{{ url('/editbill') }}">
                @csrf
                <input type="hidden" name= "id" id="id_editbillmodal" value="">
                    <div class="modal-body col-sm-12">
                        <div class="col-sm-12">
                            <b> Room Name: </b> <p id="room_name_editbillmodal"> </p>
                            <b> Bill Type: </b> <p id="bill_type_editmodal"> </p>
                        </div>
                        <div class="col-sm-12">
                            <b> Amount: </b> <input type="number" class="form-control" name="amount_editmodal" id="amount_editmodal">
                            <b> Message: </b> 
                            <textarea class="form-control" rows="5" id="message_editmodal" name="message_editmodal"> </textarea> 
                        </div>
                    </div>
                
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" name="submit">Save</button>
                    </form>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="checkBillModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                <h4 class="modal-title"> Bill Settlement </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form method="post" enctype="multipart/form-data" autocomplete="false" action="{{ url('/finishbill') }}">
                @csrf
                <input type="hidden" name= "id" id="id_checkbillmodal" value="">
                <div class="modal-body col-sm-12">
                    Are you sure the bill of <b id="room_name_checkmodal"> </b> amounting to <b id="amount_checkmodal"> </b> is paid?
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" name="submit">Yes</button>
                    </form>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_scripts')
<script>
$(document).ready(function(){

    $('#room_table').DataTable({
        'order':[[0,'desc']],
        "ordering": false,
        "info": false,
    });

    $('#bill_table').DataTable({
        'order':[[0,'desc']],
        "ordering": false,
        "info": false,
    });

    $('#tenants_table').DataTable({
        'order':[[0,'desc']],
        "ordering": false,
        "info": false,
    });

    $('.edit-room').click(function(){
        var query = $(this).attr('room-id');
        console.log(query);
        if(query != '')
        {
            // var _token = $('input[name="_token"]').val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url:"{{ route('landlord.getRoomData') }}",
                method:"POST",
                data:{
                    query:query, _token: '<?php echo csrf_token() ?>',
                    },
                success:function(data){
                    //$('#myModal').toggle('modal');
                    console.log(data);
                    $('#id_editroommodal').val(data.id);
                    $('#numtenants_editmodal').val(data.numtenants);
                    $('#isPremium_editmodal').val(data.isPremium);
                    $('#room_name_editroommodal').html(data.room_name);
                }            
            });
        }
    });

    $('.remove-room').click(function(){
        var query = $(this).attr('room-id');
        console.log(query);
        if(query != '')
        {
            // var _token = $('input[name="_token"]').val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url:"{{ route('landlord.getRoomData') }}",
                method:"POST",
                data:{
                    query:query, _token: '<?php echo csrf_token() ?>',
                    },
                success:function(data){
                    //$('#myModal').toggle('modal');
                    console.log(data);
                    $('#id_removeroommodal').val(data.id);
                    $('#room_name_removeroommodal').html("Room "+data.room_name);
                }            
            });
        }
    });

    $('.edit-tenant').click(function(){
        var query = $(this).attr('tenant-id');
        console.log(query);
        if(query != '')
        {
            // var _token = $('input[name="_token"]').val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url:"{{ route('landlord.getTenantData') }}",
                method:"POST",
                data:{
                    query:query, _token: '<?php echo csrf_token() ?>',
                    },
                success:function(data){
                    //$('#myModal').toggle('modal');
                    console.log(data);
                    $('#id_edittenantmodal').val(data.id);
                    document.getElementById('startdate_edittenantmodal').value = data.start_date;
                    document.getElementById('enddate_edittenantmodal').value = data.end_date;
                    $('#gender_edittenantmodal').html(data.gender);
                    document.getElementById('birthdate_edittenantmodal').value = data.birthday;
                    $('#tenant_name_edittenantmodal').html(data.last_name + ", " + data.first_name);
                    $('#contactnumber_edittenantmodal').val(data.contact_number);
                }            
            });
        }
    });

    $('.remove-tenant').click(function(){
        var query = $(this).attr('tenant-id');
        console.log(query);
        if(query != '')
        {
            // var _token = $('input[name="_token"]').val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url:"{{ route('landlord.getTenantData') }}",
                method:"POST",
                data:{
                    query:query, _token: '<?php echo csrf_token() ?>',
                    },
                success:function(data){
                    //$('#myModal').toggle('modal');
                    console.log(data);
                    $('#id_removetenantmodal').val(data.id);
                    $('#tenant_removetenantmodal').html("Tenant "+data.first_name+ " " + data.last_name);
                }            
            });
        }
    });

    $('.edit-bill').click(function(){
        var query = $(this).attr('bill-id');
        console.log(query);
        if(query != '')
        {
            // var _token = $('input[name="_token"]').val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url:"{{ route('landlord.getBillData') }}",
                method:"POST",
                data:{
                    query:query, _token: '<?php echo csrf_token() ?>',
                    },
                success:function(data){
                    //$('#myModal').toggle('modal');
                    console.log(data);
                    $('#id_editbillmodal').val(data.id);
                    $('#room_name_editbillmodal').html(data.room_name);
                    $('#bill_type_editmodal').html(data.bill_type);
                    $('#amount_editmodal').val(data.amount);
                    $('#message_editmodal').val(data.message);
                }            
            });
        }
    });

    $('.check-bill').click(function(){
        var query = $(this).attr('bill-id');
        console.log(query);
        if(query != '')
        {
            // var _token = $('input[name="_token"]').val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url:"{{ route('landlord.getBillData') }}",
                method:"POST",
                data:{
                    query:query, _token: '<?php echo csrf_token() ?>',
                    },
                success:function(data){
                    //$('#myModal').toggle('modal');
                    console.log(data);
                    $('#id_checkbillmodal').val(data.id);
                    $('#room_name_checkmodal').html("Room "+data.room_name);
                    $('#amount_checkmodal').html(data.amount);
                }            
            });
        }
    });

    $('#tenant_status').change(function(){
        var query = $(this).val();
        var innerHTML = "";
        console.log(query);
        if(query != '')
        {
            // var _token = $('input[name="_token"]').val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url:"{{ route('landlord.getTenantGroup') }}",
                method:"POST",
                data:{
                    query:query, _token: '<?php echo csrf_token() ?>',
                    },
                success:function(data){
                    console.log(data);
                    $('#tbody-tenant').empty();
                    if(data.length == 0){
                        if(query == 1){
                            query = "active";
                        }else{
                            query = "inactive";
                        }
                        innerHTML += "<tr> <td>No tenant is "+query+"</td>"+
                        "<td>-</td>"+
                        "<td>-</td>"+
                        "<td>-</td>"+
                        "<td>-</td>"
                        +"</tr>";
                    }else{
                        for(var i = 0; i != data.length;i++){
                            console.log(data);
                            innerHTML += "<tr> <td>"+ data[i].room_name+"</td>"+
                            "<td>"+ data[i].last_name+"</td>"+
                            "<td>"+ data[i].first_name+"</td>"+
                            "<td>"+ data[i].contact_number+"</td>"+
                            "<td> <i class='fa fa-edit edit' tenant-id='"+data[i].id+"' style='font-size:19px' data-toggle='modal' data-target='#editModal'></i>"+
                                    "<i class='fa fa-check check' tenant-id='"+data[i].id+"' style='font-size:19px' data-toggle='modal' data-target='#removeModal'></i></td>"+
                            "</tr>";
                        }
                    }
                    document.getElementById('tbody-tenant').innerHTML = innerHTML;
                    console.log(innerHTML);
                }
            });
        }
    });


    $('#bills_status').change(function(){
        var query = $(this).val();
        var innerHTML = "";
        console.log(query);
        if(query != '')
        {
            // var _token = $('input[name="_token"]').val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url:"{{ route('landlord.getBillGroup') }}",
                method:"POST",
                data:{
                    query:query, _token: '<?php echo csrf_token() ?>',
                    },
                success:function(data){
                    console.log(data);
                    $('#tbody-bills').empty();
                    if(data.length == 0){
                        if(query == "Paid"){
                            query = "paid";
                        }else{
                            query = "pending";
                        }
                        innerHTML += "<tr> <td>No bill is "+query+"</td>"+
                        "<td>-</td>"+
                        "<td>-</td>"+
                        "<td>-</td>"+
                        "<td>-</td>"
                        +"</tr>";
                    }else{
                        for(var i = 0; i != data.length;i++){
                            console.log(data);
                            innerHTML += "<tr> <td>"+ data[i].room_name+"</td>"+
                            "<td>"+ data[i].bill_type+"</td>"+
                            "<td>"+ data[i].amount+"</td>"+
                            "<td>"+ data[i].message+"</td>"+
                            "<td> <i class='fa fa-edit edit' tenant-id='"+data[i].id+"' style='font-size:19px' data-toggle='modal' data-target='#editModal'></i>"+
                                    "<i class='fa fa-check check' tenant-id='"+data[i].id+"' style='font-size:19px' data-toggle='modal' data-target='#removeModal'></i></td>"+
                            "</tr>";
                        }
                    }
                    document.getElementById('tbody-bills').innerHTML = innerHTML;
                    console.log(innerHTML);
                }
            });
        }
    });


    $('#room_status').change(function(){
        var query = $(this).val();
        var innerHTML = "";
        console.log(query);
        if(query != '')
        {
            // var _token = $('input[name="_token"]').val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url:"{{ route('landlord.getRoomGroup') }}",
                method:"POST",
                data:{
                    query:query, _token: '<?php echo csrf_token() ?>',
                    },
                success:function(data){
                    console.log(data);
                    $('#tbody-rooms').empty();
                    if(data.length == 0){
                        if(query == 0){
                            query = "inactive";
                        }else{
                            query = "active";
                        }
                        innerHTML += "<tr> <td>No room is "+query+"</td>"+
                        "<td>-</td>"+
                        "<td>-</td>"+
                        "<td>-</td>"+
                        "<td>-</td>"
                        +"</tr>";
                    }else{
                        for(var i = 0; i != data.length;i++){
                            console.log(data);
                            innerHTML += "<tr> <td>"+ data[i].room_name+"</td>"+
                            "<td>"+ data[i].isPremium+"</td>"+
                            "<td>"+ data[i].max_tenants+"</td>"+
                            "<td>"+ data[i].curr_tenants+"</td>"+
                            "<td> <i class='fa fa-edit edit' tenant-id='"+data[i].id+"' style='font-size:19px' data-toggle='modal' data-target='#editModal'></i>"+
                                    "<i class='fa fa-check check' tenant-id='"+data[i].id+"' style='font-size:19px' data-toggle='modal' data-target='#removeModal'></i></td>"+
                            "</tr>";
                        }
                    }
                    document.getElementById('tbody-rooms').innerHTML = innerHTML;
                    console.log(innerHTML);
                }
            });
        }
    });
});

</script>

@endsection
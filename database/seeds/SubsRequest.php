<?php

use Illuminate\Database\Seeder;
use App\Landlord;
use App\Room;


class SubsRequest extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subscription_request')->insert([
            'landlord_id' =>Landlord::where('first_name','client')->where('last_name','test')->first()->id,
            'room_id' => Room::where('room_number','2306')->first()->id,
            'status'=>1,
        ]);

        DB::table('subscription_request')->insert([
            'landlord_id' =>Landlord::where('first_name','client')->where('last_name','test')->first()->id,
            'room_id' => Room::where('room_number','2307')->first()->id,
            'status'=>1,
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Building;
use Carbon\Carbon;

class Landlord extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('landlord_profile')->insert([
            'user_id' => User::where('username','client')->first()->id,
            'first_name' => 'client',
            'last_name' => 'test',
            'contact_number' => '2307',
            'gender' => 'Male',
            'birthday' => Carbon::create('2019','03','03'),
            'building_id' => Building::where('name','PSCE')->first()->id,
        ]);
    }
}

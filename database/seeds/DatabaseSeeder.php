<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTypeSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(AdminProfileSeeder::class);

        $this->call(Building::class);
        $this->call(Rooms::class);
        $this->call(Landlord::class);
        
        $this->call(SubsRequest::class);
        $this->call(Tenant::class);
        
        $this->call(messageSeeder::class);

    }
}

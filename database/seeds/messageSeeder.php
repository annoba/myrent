<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Tenant;

class messageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('table_room_message')->insert([
            'tenant_userid' => User::where('username','secondclient')->first()->id,
            'message' => 'hello',
            'room_id' =>1,
            'sender'=>'tenant',
        ]);
    }
}

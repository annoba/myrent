<?php

use Illuminate\Database\Seeder;
use App\Room;
use App\User;
use Carbon\Carbon;

class Tenant extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tenant_profile')->insert([
            'user_id' => User::where('username','secondclient')->first()->id,
            'first_name' => 'secondclient',
            'last_name' => 'test',
            'contact_number' => '09278002645',
            'gender' => 'Male',
            'birthday' => Carbon::create('2019','03','03'),
            'start_date' => Carbon::create('2019','03','03'),
            'end_date' => Carbon::create('2019','03','03'),
            'room_id' => Room::where('room_number','2306')->first()->id,
        ]);
    }
}

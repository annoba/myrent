<?php

use Illuminate\Database\Seeder;

class UserTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_type')->insert([
            'type' => 'Admin',
            'description' => 'Manages the website',
        ]);

        DB::table('user_type')->insert([
            'type' => 'Landlord',
            'description' => 'Manages the place',
        ]);

        DB::table('user_type')->insert([
            'type' => 'Tenant',
            'description' => 'Rents the place',
        ]);
    }
}

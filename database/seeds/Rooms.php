<?php

use Illuminate\Database\Seeder;
use App\Building;

class Rooms extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('building_rooms')->insert([
            'building_id' => Building::where('name','PSCE')->first()->id,
            'room_number' => '2306',
            'isPremium'=>1,
            'max_tenants'=>4,
            'isActive'=>1,
        ]);
        DB::table('building_rooms')->insert([
            'building_id' => Building::where('name','PSCE')->first()->id,
            'room_number' => '2307',
            'isPremium'=>1,
            'max_tenants'=>4,
            'isActive'=>1,
        ]);
        
    }
}

<?php

use Illuminate\Database\Seeder;
use App\UserType;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => 'annoba',
            'email' => 'annoba@myrent.com',
            'password' => bcrypt('mrpassword'),
            'isActive' => 1,
            'avatar'=>'default.png',
            'user_type' =>  UserType::where('type', 'Admin')->first()->id
        ]);

        DB::table('users')->insert([
            'username' => 'marktabbu',
            'email' => 'mark@myrent.com',
            'password' => bcrypt('asd'),
            'isActive' => 1,
            'avatar'=>'default.png',
            'user_type' =>  UserType::where('type', 'Admin')->first()->id
        ]);

        DB::table('users')->insert([
            'username' => 'client',
            'email' => 'client@myrent.com',
            'password' => bcrypt('dsa'),
            'isActive' => 1,
            'avatar'=>'default.png',
            'user_type' =>  UserType::where('type', 'Landlord')->first()->id
        ]);

        DB::table('users')->insert([
            'username' => 'secondclient',
            'email' => 'secondclient@myrent.com',
            'password' => bcrypt('asd'),
            'isActive' => 1,
            'avatar'=>'default.png',
            'user_type' =>  UserType::where('type', 'Tenant')->first()->id
        ]);
    }
}

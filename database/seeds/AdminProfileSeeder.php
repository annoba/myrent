<?php

use Illuminate\Database\Seeder;
use App\User;

class AdminProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_profile')->insert([
            'last_name' => 'Oba',
            'first_name' => 'Ann Bernadette',
            'contact_number' => '09278002645',
            'user_id' => User::where('email', '=', 'annoba@myrent.com')->first()->id,
            'added_by' => User::where('email', '=', 'annoba@myrent.com')->first()->id,
        ]);

        DB::table('admin_profile')->insert([
            'last_name' => 'Tabbu',
            'first_name' => 'Mark Daniel',
            'contact_number' => '09655727687',
            'user_id' => User::where('email', '=', 'mark@myrent.com')->first()->id,
            'added_by' => User::where('email', '=', 'mark@myrent.com')->first()->id,
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class Building extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('client_building')->insert([
            'name' => 'PSCE',
            'address' => 'bank drive ortigas',
            'contact_number' => '09278002645',
            'building_type' => "Condo",
        ]);
    }
}

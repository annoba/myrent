<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillPerRoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bill_per_room', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('bill_type');
            $table->string('amount');
            $table->string('billed_to');
            $table->string('status');
            $table->string('start_date');
            $table->string('end_date');
            $table->string('message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bill_per_room');
    }
}

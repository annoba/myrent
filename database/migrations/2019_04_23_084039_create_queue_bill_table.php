<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQueueBillTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('queue_bill', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('amount');
            $table->string('frequency');
            $table->string('every');
            $table->date('start_date');
            $table->date('end_date');
            $table->integer('billed_to');
            $table->integer('bill_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('queue_bill');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillPerTenantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bill_per_tenant', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('bill_per_room_id');
            $table->string('amount');
            $table->string('billed_to');
            $table->integer('assigned_by');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bill_per_tenant');
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
Route::post('/login','Auth\LoginController@login')->name('login');

Route::get('/','HomeController@welcome')->name('landing');
Route::get('/dashboard', 'HomeController@index')->name('dashboard');
   
Route::get('/user/profile','HomeController@Profile')->name('profile');

Route::post('/view-message','LandlordController@getMyMessage')->middleware('auth');
Route::post('/recieve-msg','HomeController@getReceiveMessage')->middleware('auth');
// Route::get('/view-message/{id}','LandlordController@getMyMessage')->name('my.message')->middleware('auth');


Route::post('/send-message','LandlordController@sendMessage')->middleware('auth');


Route::group(['middleware' => 'admin'], function() {
    Route::get('/admin/client','AdminController@client')->name('admin.client');
    Route::get('/admin/client/add','AdminController@addclient')->name('add.client');
    Route::post('/admin/client/add/data','AdminController@insertClient')->name('send.add.client');
    Route::get('/admin/subscription-pending','AdminController@subsViewPending')->name('view.subs.pending');
    Route::get('/admin/subscription-all','AdminController@subsView')->name('view.subs.all');
    Route::post('/admin/accept-req','AdminController@acceptPending')->name('request.action.subs');
    Route::get('/admin/admin-accounts','AdminController@adminAccounts')->name('admin.accounts');
    Route::get('/admin/add-account','AdminController@addAccount')->name('add.admin.acc');
    Route::post('/admin-insert-account','AdminController@insertNewAccount')->name('insert.account');
    
 
});

Route::group(['middleware' => 'landlord'], function() {
    Route::get('/rooms','LandlordController@gotoRooms')->name('landlord.room');
    Route::get('/tenants','LandlordController@gotoTenants')->name('landlord.tenant');
    Route::get('/addrooms','RoomController@gotoAddRooms')->name('landlord.addroom');
    Route::post('/addrooms','RoomController@addRooms')->name('landlord.addroom');
    Route::get('/addtenants','TenantController@gotoAddTenants')->name('landlord.addtenant');
    Route::post('/addtenants','TenantController@addTenants')->name('landlord.addtenant');
    Route::post('/postAnnouncement','AnnouncementController@postAnnouncement');
    Route::post('/getrooms','RoomController@getRooms')->name('landlord.getRooms');
    Route::get('/records','HomeController@gotoRecords')->name('landlord.records');

    Route::post('/edit-profile/landlord','LandlordController@editProfile')->name('edit.prof.landlord');
    Route::get('/bills','BillController@gotoBill')->name('landlord.bill');
    Route::get('/billtypes','BillController@gotoTypeBill')->name('landlord.typebill');
    Route::post('/billtypes','BillController@addBillType')->name('landlord.addtypebill');
    Route::post('/bills','BillController@postBill')->name('landlord.postbill');
    Route::post('/getbills','BillController@getBillData')->name('landlord.getBillData');
    Route::post('/editbill','BillController@editBill');
    Route::post('/finishbill','BillController@finishBill');
    Route::post('/getroomsdata','RoomController@getRoomData')->name('landlord.getRoomData');
    Route::post('/editroom','RoomController@editRoom');
    Route::post('/removeroom','RoomController@removeRoom');
    Route::post('/gettenants','TenantController@getTenantData')->name('landlord.getTenantData');
    Route::post('/gettenantgroup','TenantController@getTenantGroup')->name('landlord.getTenantGroup');
    Route::post('/getbillgroup','BillController@getBillGroup')->name('landlord.getBillGroup');
    Route::post('/edittenant','TenantController@editTenant');
    Route::post('/removetenant','TenantController@removeTenant');
    Route::post('/getroomgroup','RoomController@getRoomGroup')->name('landlord.getRoomGroup');
    Route::get('message/landlord','LandlordController@getAllUsersRoomRedirect')->name('view.message.landlord');
    Route::get('message/landlord/{id}','LandlordController@getAllUsersRoom')->name('view.message');

});

Route::group(['middleware' => 'tenant'], function() {
    Route::get('/see-more/{id}','TenantController@seeMore');
    Route::post('/edit-profile','TenantController@editProfile')->name('edit.prof');
    Route::get('/view-bills','TenantController@viewBills')->name('view.bills');
    Route::get('/splitbills','BillController@viewSplitBills')->name('view.splitbills');
    Route::get('message/tenant','TenantController@getMyMessage')->name('view.message.tenant');
    Route::post('/getBill','TenantController@getBills')->name('tenant.getBills');
    Route::post('/getPendingBill','TenantController@getPendingBills')->name('tenant.getPendingBills');
    Route::post('/approvePendingBill','TenantController@approvePendingBill')->name('tenant.approvePendingBill');

});

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Auth;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function maxAttempts()
    {
        return property_exists($this, 'maxAttempts') ? $this->maxAttempts : 2;
    }
    public function decayMinutes()
    {
        return property_exists($this, 'decayMinutes') ? $this->decayMinutes : 1;
    }

    public function redirect(){
        return 'hey';
        if(Auth::user()->user_type == 1){
            return redirect(route('dashboard'));
        }else if (Auth::user()->user_type == 2){
            return redirect(route('landlord.dashboard'));
        }else if(Auth::user()->user_type == 3){
            return redirect(route('tenant.dashboard'));
        }
    }

    public function login(Request $request){
        $Usermail=$request->input('usermail');
        $userdata=User::where('Username',$Usermail)
                        ->orWhere('email',$Usermail)->get();
    
        if(count($userdata)>0)
        {
            // LOGIN ATTEMPTS
            // if($this->hasTooManyLoginAttempts($request))
            // {
            //     return back()->withErrors('toomanylog');
            // }

            if ($this->attemptLogin($request)) {
                return $this->sendLoginResponse($request);
            }

            $password=Hash::check($request->input('password'),$userdata->first()->password);
            if($password)
            {
                Auth::loginUsingId($userdata->first()->id);
                return redirect(route('dashboard'));
                
            }
            else
            {
                // $this->incrementLoginAttempts($request);
                return back()->withErrors('incorrectpass');
            }
        }
        else{
           return back()->withErrors('usermail');
        }
    }
}

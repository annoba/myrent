<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Room;
use Auth;
use App\Tenant;
use App\SubsRequest;

class RoomController extends Controller
{

    public function gotoAddRooms(){
        $data=array();
        $id = Auth::user()->landlord_profile->building_id;
        $rooms = Room::where('building_id','=',$id)->get();

        return view('landlord.add_room')->with('rooms',$rooms);
    }

    public function addRooms(Request $request){
        $rooms_saved = "";
        $duplicate_room_name = "";
        for($i = 0; $i < $request->room_count; $i++) {
            if(Room::where([['room_number','=',$request->room_name[$i]],['building_id','=',Auth::user()->landlord_profile->building_id]])->exists()){
                $duplicate_room_name.=$request->room_name[$i];
            }else{
                $room  = new Room;
                $room->room_number= $request->room_name[$i];
                $room->isPremium = $request->is_premium[$i];
                $room->building_id = Auth::user()->landlord_profile->building_id;
                $room->max_tenants = $request->room_maxtenants[$i];
                $room->isActive = 1;
                
                

                if($room->save()){

                    $subreq=new SubsRequest;
                    $subreq->landlord_id=auth()->user()->landlord_profile->id;
                    $subreq->room_id=$room->id;
                    if($request->is_premium[$i]==1){      
                        $subreq->status=1;

                        
                    }else{
                        $subreq->status=2;
                    }
                    $subreq->save();

                    $rooms_saved.=$room->room_number;
    
                    if($request->room_count != 1){
                        if($i != $request->room_count-1) {
                            $rooms_saved.=', ';
                        }else{
                            if($duplicate_room_name == ""){
                                return redirect()->back()->with('success', 'Room/s '.$rooms_saved.' has been saved');
                            }else{
                                return redirect()->back()->with('success', 'Room/s '.$rooms_saved.' has been saved, but '.$duplicate_room_name.' already exist.');
                            }
                        }
                    }else{
                        return redirect()->back()->with('success', 'Room '.$rooms_saved.' has been saved');
                    }
                }
            }
        }
    }

    public function getRooms(Request $request){
        $id = Auth::user()->landlord_profile->building_id;
        $data = array();
        $query = $request->get('query');
        foreach(Room::where('building_id','=',$id)->get() as $room){
            if(strpos($room->room_number, $query) !== false){
                $data[] = $room;
            }
        } 
       
        return $data;
    }

    public function getRoomData(Request $request){
        $data = array();
        $room = Room::where('id','=',$request['query'])->first();

        $data = [
            'id' => $room->id,
            'room_name' => $room->room_number,
            'isPremium' => $room->isPremium,
            'numtenants' => $room->max_tenants
        ];
        return $data;
    }

    public function removeRoom(Request $request){
        $room = Room::where('id','=',$request->id)->first();
        $room->isActive = 0;

        if($room->save()){
            return redirect()->back()->with('success', 'Room '.$room->room_number .' updated.');
        }else{
            return redirect()->back()->with('error', 'Room failed to update.');
        }
    }

    public function editRoom(Request $request){
        $room = Room::where('id','=',$request->id)->first();
        $room->max_tenants = $request->numtenants_editmodal;
        // $room->isPremium = $request->is_premium_editmodal;

        $req=SubsRequest::where('landlord_id',auth()->user()->landlord_profile->id)
                        ->where('room_id',$request->id)
                        ->first();
        if($request->is_premium_editmodal==1){ 
            $req->status=1;
            $room->isPremium=1;    
        }else{
            $req->status=2;
            $room->isPremium=2;
        }

        $req->save();

        // return $req;
        if($room->save()){
            return redirect()->back()->with('success', 'Room '.$room->room_number .' updated.');
        }else{
            return redirect()->back()->with('error', 'Room failed to update.');
        }
    }

    public function getRoomGroup(Request $request){
        $rooms = array();
        $id = Auth::user()->landlord_profile->building_id;
        $tenants = array();     
        foreach(Room::where([['building_id','=',$id],['isActive','=',$request->get('query')]])->get() as $room){
            $tenants = array();
            if(Tenant::where([['room_id','=',$room->id]])->count() != 0){
                foreach(Tenant::where([['room_id','=',$room->id]])->get() as $tenant){
                    $isActive = $tenant->user->isActive;
                    if($isActive == "1"){
                        $tenants[] = $tenant;
                    }
                }
            }else{
                $tenants = [];
            }
            
            $rooms[] = array(
                'room_name' => $room->room_number,
                'isPremium' => $room->isPremium,
                'max_tenants' => $room->max_tenants,
                'id' => $room->id,
                'curr_tenants' => count($tenants),
            );
        }
        return $rooms;
    }

}

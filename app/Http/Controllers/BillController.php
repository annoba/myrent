<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BillType;
use App\Bill;
use App\Room;
use App\Tenant;
use Auth;
use App\BillPerTenant;
use App\QueueBill;

class BillController extends Controller
{
    public function gotoBill(){
        $data_array = array();
        $bill_array = array();
        $id = Auth::user()->landlord_profile->building_id;
        foreach(Room::where('building_id','=',$id)->get() as $room) {
            foreach(Bill::where([['status','=','Pending'],['billed_to','=',$room->id]]) as $bill){
                $bill_array[] = $bill;
            } 
        }

        $bill_types = BillType::where([['building_id','=', Auth::user()->landlord_profile->building_id],['isActive','=',1]])->get();
        $data = [
            'bill_types' => $bill_types,
            'bills' => $bill_array,
        ];
        return view('landlord.bill')->with('data', $data);
    }

    public function gotoTypeBill(){
        $bill_types = BillType::where([['building_id','=', Auth::user()->landlord_profile->building_id],['isActive','=',1]])->get();
        return view('landlord.bill_type')->with('bill_types', $bill_types);
    }

    public function addBillType(Request $request){
        $bill_type = new BillType;
        $bill_type->type = $request->bill_type;
        $bill_type->description = $request->bill_description;
        $bill_type->building_id = Auth::user()->landlord_profile->building_id;
        $bill_type->isActive = 1;

        if($bill_type->save()){
            return redirect()->back()->with('success', 'Bill Type added.');
        }else{
            return redirect()->back()->with('error', 'Bill Type failed to be added.');
        }
    }

    public function postBill(Request $request){
            $bill = new Bill;
            $bill->bill_type = $request->type_of_bill;
            $bill->amount = $request->amount;
            $bill->billed_to = Room::where([['room_number','=',$request->room_name],['building_id','=',Auth::user()->landlord_profile->building_id]])->first()->id;
            $bill->status = "Pending";
            $bill->start_date = $request->start_date;
            $bill->end_date = $request->end_date;
            $bill->message = $request->bill_message;
            $bill->save();

            $room = Room::where([['room_number','=',$request->room_name],['building_id','=',Auth::user()->landlord_profile->building_id]])->first();
            $tenants = Tenant::where('room_id','=',$room->id)->get();
            $divider = count($tenants);
            if($divider == 0){
                return redirect()->back()->with('error', 'No tenant in the room.');        
            }else{
                $per_tenant = $bill->amount / $divider;

                foreach($tenants as $tenant){
                    $bill_per_tenant = new BillPerTenant;
                    $bill_per_tenant->bill_per_room_id = $bill->id;
                    $bill_per_tenant->amount = $per_tenant;
                    $bill_per_tenant->assigned_by = Auth::user()->landlord_profile->id;
                    $bill_per_tenant->billed_to = $tenant->id;
                    if(count(Room::where([['room_number','=',$request->room_name],['building_id','=',Auth::user()->landlord_profile->building_id],['isPremium','=','0']])->get()) == 1){
                        $bill_per_tenant->status = "Final";
                    }else if(count(Room::where([['room_number','=',$request->room_name],['building_id','=',Auth::user()->landlord_profile->building_id],['isPremium','=','0']])->get()) > 1){
                        $bill_per_tenant->status = "Pending";
                    }else{
                        $bill_per_tenant->status = "Pending";
                    }
                    $bill_per_tenant->save();
                }
                return redirect()->back()->with('success', 'Bill posted.');  
            }
                  
    }

    public function getBillData(Request $request){
        // return $request['query'];
        $data = array();
        $bill = Bill::where('id','=',$request['query'])->first();

        $data = [
            'id' => $bill->id,
            'room_name' => $bill->room->room_number,
            'bill_type' => $bill->billing_type->type,
            'amount' => $bill->amount,
            'message' => $bill->message,
        ];
        return $data;

    }

    public function editBill(Request $request){
        $bill = Bill::where('id','=',$request->id)->first();
        $bill->amount = $request->amount_editmodal;
        $bill->message = $request->message_editmodal;

        if($bill->save()){
            return redirect()->back()->with('success', 'Bill updated.');
        }else{
            return redirect()->back()->with('error', 'Bill failed to update.');

        }
    }

    public function finishBill(Request $request){
        $bill = Bill::where('id','=',$request->id)->first();
        $bill->status = "Paid";

        if($bill->save()){
            return redirect()->back()->with('success', 'Bill paid.');
        }else{
            return redirect()->back()->with('error', 'Bill failed to update.');

        }
    }

    public function getBillGroup(Request $request){
        $bills = array();
        $id = Auth::user()->landlord_profile->building_id;
        $data = array();
        foreach(Room::where('building_id','=',$id)->get() as $room) {
            foreach(Bill::where([['status','=',$request->get('query')],['billed_to','=',$room->id]])->get() as $bill){
                $bills[] = array(
                    'room_name' => $bill->room->room_number,
                    'bill_type' =>$bill->billing_type->type,
                    'amount' => $bill->amount,
                    'id' => $bill->id,
                    'message' => $bill->message
                );
            } 
        }
        return $bills;
    }

    public function viewSplitBills(){
        $tenants = Tenant::where('room_id','=',Auth::user()->tenant_profile->room_id)->get();
    }
}

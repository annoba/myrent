<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tenant;
use App\Building;
use App\User;
use App\UserType;
use App\Announcement;
use App\Room;
use Auth;
use App\messages;
use Validator;
use App\BillType;
use App\BillPerTenant;
use App\Bill;

class TenantController extends Controller
{
    public function seeMore($id){
        return view('tenant.home')->with('data',Announcement::join('landlord_profile','postedBy','=','landlord_profile.id')
                                                            ->join('building_rooms','landlord_profile.building_id','=','building_rooms.building_id')
                                                            ->select('announcement.created_at','title','body','level_of_urgency')
                                                            ->where('building_rooms.id',auth()->user()->tenant_profile->room_id)->take($id+4)->orderBy('announcement.id','desc')->get());
    }

    public function viewBills(){
        $data = array();
        $tenants = array();
        $room = Room::where('id','=',Auth::user()->tenant_profile->room_id)->first();
        $building_id = Room::where('id','=',Auth::user()->tenant_profile->room_id)->first()->building_id;
        $bill_types = BillType::where([['building_id','=', $building_id],['isActive','=',1]])->get();
        if(Tenant::where([['room_id','=',Auth::user()->tenant_profile->room_id]])->count() != 0){
            foreach(Tenant::where([['room_id','=',Auth::user()->tenant_profile->room_id]])->get() as $tenant){
                $isActive = $tenant->user->isActive;
                if($isActive == "1"){
                    $tenants[] = $tenant;
                }
            }
        }else{
            $tenants = [];
        }

        // return $room['isPremium'];

        $data = [
            'bill_types' => $bill_types,
            'tenant_count' => count($tenants),
            'room' => $room
        ];
        // return $data['bill_types'][0]['type'];
        return view('tenant.bills')->with('data', $data);
    }

    public function getBills(Request $request){
        $bill_per_tenant = array();
        $bills = Bill::where([['billed_to','=',Auth::user()->tenant_profile->room_id],['bill_type','=',$request->get('query')],['status','=','Pending']])->get();
        
            foreach($bills as $bill){
                $temps = BillPerTenant::where('bill_per_room_id','=',$bill->id)->get();
                foreach($temps as $temp){
                    // return $temp->bill->billing_type->type;
                    $bill_per_tenant[] = [
                        'amount' => $temp->amount,
                        'billed_to' => $temp->tenant->last_name.','.$temp->tenant->first_name,
                        'type' => $temp->bill->billing_type->type,
                        'id' => $temp->id,
                        'inclusive_date' => $temp->bill->start_date.' - '.$temp->bill->end_date,
                        'status' => $temp->status
                    ];
                }
            }
        
        
        return $bill_per_tenant;
    }

    public function getPendingBills(Request $request){
        $bills = Bill::where([['billed_to','=',Auth::user()->tenant_profile->room_id],['bill_type','=',$request->get('query')],['status','=','Pending']])->get();
        foreach($bills as $bill){
            $temps = BillPerTenant::where([['bill_per_room_id','=',$bill->id],['status','=',"Pending"]])->get();
            foreach($temps as $temp){
                // return $temp->bill->billing_type->type;
                $bill_per_tenant[] = [
                    'amount' => $temp->amount,
                    'billed_to' => $temp->tenant->last_name.','.$temp->tenant->first_name,
                    'type' => $temp->bill->billing_type->type,
                    'id' => $temp->id,
                    'inclusive_date' => $temp->bill->start_date.' - '.$temp->bill->end_date,
                    'status' => $temp->status

                ];
            }
        }
        return $bill_per_tenant;
    }

    function approvePendingBill(Request $request){
        $bills = Bill::where([['billed_to','=',Auth::user()->tenant_profile->room_id],['bill_type','=',$request->get('query')],['status','=','Pending']])->get();
        foreach($bills as $bill){
            $temps = BillPerTenant::where([['bill_per_room_id','=',$bill->id],['status','=','Pending']])->get();
            foreach($temps as $temp){
                $temp2 = BillPerTenant::where('id','=',$temp->id)->first();
                $temp2->status = "Final";
                $temp2->save();
            }
        }
        return "Success";
    }

    public function getMyMessage(){
        return view('tenant.message')->with('rooms',Messages::getMyMessage(auth()->user()->id));   

    }

    public function editProfile(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'Firstname'=>'required|max:50',
                'Lastname'=>'required|max:50',
                'contnum'=>'required',
                'username'=>'required|max:20|unique:users,username,'.auth()->user()->id,
                'mail'=>'required|email|max:30|unique:users,email,'.auth()->user()->id,
                'bdate'=>'required',
                'sdate'=>'required',
                'edate'=>'required',
                'gender'=>'required',
            ]);

            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator->errors());
            }else{
                return Tenant::updateTenant($request);
            }
        }catch(Exeption $e){
            return back()->withErrors($e);
        }
    }

    public function gotoAddTenants(){
        $id = auth()->user()->landlord_profile->building_id;
        $data = array();
        foreach(Building::where('id','=',$id)->get() as $building) {
            foreach($building->rooms->all() as $room){
                foreach($room->tenant->all() as $tenant){
                    $data[] = $tenant;
                }
            } 
        }
        return view('landlord.add_tenant')->with('tenant_data',$data);
    }

    public function addTenants(Request $request){
        $tenants_saved = "";
        $duplicate_username = "";
        for($i = 0; $i < $request->tenant_count; $i++) {

            if(User::where([['username','=',$request->username[$i]],['isActive','=',1]])->exists()){
                $duplicate_username .= $request->username[$i];
            }else{
                $user = new User;
                $user->email = $request->email[$i];
                $user->password = bcrypt($request->password[$i]);
                $user->username = $request->username[$i];
                $user->user_type = UserType::where('type','=','Tenant')->first()->id;
                $user->isActive = 1;
                $user->avatar = "default.png";

                $user->avatar='default.png';
                
                if($user->save()){
                    $tenant  = new Tenant;
                    $tenant->user_id = $user->id;
                    $tenant->last_name = $request->last_name[$i];
                    $tenant->first_name = $request->first_name[$i];
                    $tenant->birthday = $request->birth_date[$i];
                    $tenant->contact_number = $request->contact_number[$i];
                    $tenant->gender = $request->gender[$i];
                    $tenant->start_date = $request->start_date[$i];
                    $tenant->end_date = $request->end_date[$i];
                    $tenant->room_id = Room::where([['room_number','=',$request->room_id[$i]],['building_id', '=', Auth::user()->landlord_profile->building_id]])->first()->id;
        
                    if($tenant->save()){
                        $tenants_saved.= $tenant->last_name .', '.$tenant->first_name;
                        
                        if($request->tenant_count != 1){
                            if($i != $request->tenant_count-1) {
                                $tenants_saved.='; ';
                            }else{
                                return redirect()->back()->with('error', 'Error saving all tenant. Tenant/s '.$tenants_saved.' are saved, and Tenant/s '.$duplicate_username.' already exist.');
                            }
                        }
                    }else{
                        $user = User::where([['username','=',$request->username[$i]],['isActive','=',1]])->first();
                        $user->delete();
                        return redirect()->back()->with('error', 'Error something went wrong.');
                    }      
                }else{
                    $user = User::where([['username','=',$request->username[$i]],['isActive','=',1]])->first();
                    $user->delete();
                    return redirect()->back()->with('error', 'Error something went wrong.');
                }
            }
        }
        return redirect()->back()->with('success', 'Tenant/s '.$tenants_saved.' have been saved');
    }

    public function getTenantData(Request $request){
        $data = array();
        $tenant = Tenant::where('id','=', $request['query'])->first();
        
        return $tenant;
    }

    public function getTenantGroup(Request $request){
        $tenants =  array();
        $id = Auth::user()->landlord_profile->building_id;
        $data = array();
        foreach(Building::where('id','=',$id)->get() as $building) {
            foreach($building->rooms->all() as $room){
                foreach($room->tenant->all() as $tenant){
                    if($tenant->user->isActive == $request->get('query')){
                        $tenants[] = array(
                            'id' => $tenant->id,
                            'room_name' => $tenant->room->room_number,
                            'last_name' => $tenant->last_name,
                            'first_name' => $tenant->first_name,
                            'contact_number' => $tenant->contact_number,
                            'start_date' => $tenant->start_date,
                            'end_date' => $tenant->end_date,
                            'gender' => $tenant->gender,
                            'birthday' => $tenant->birthday,
                        );
                    }
                }
            } 
        }
        return $tenants;
    }  

    public function editTenant(Request $request){
        $tenant = Tenant::where('id', '=', $request->id)->first();
        $tenant->contact_number = $request->contactnumber_edittenantmodal;
        $tenant->start_date = $request->startdate_edittenantmodal;
        $tenant->end_date = $request->enddate_edittenantmodal;

        if($tenant->save()){
            return redirect()->back()->with('success', 'Tenant information have been saved');
        }else{
            return redirect()->back()->with('error', 'Tenant information failed to save');
        }
    }

    public function removeTenant(Request $request){
        $tenant = Tenant::where('id', '=', $request->id)->first();
        $user = User::where('id','=',$tenant->user_id)->first();
        $user->isActive = 0;
        if($user->save()){
            return redirect()->back()->with('success', 'Tenant removed successfully');
        }else{
            return redirect()->back()->with('error', 'Tenant removal failed');
        }
    }
}

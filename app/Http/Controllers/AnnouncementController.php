<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Announcement;
use Auth;

class AnnouncementController extends Controller
{
    public function postAnnouncement(Request $request){
        $announcement = new Announcement;
        $announcement->title = $request->title;
        $announcement->postedBy = Auth::user()->landlord_profile->id;
        $announcement->body = $request->body;
        $announcement->level_of_urgency = $request->level_of_urgency;
        if($announcement->save()){
            return redirect()->back()->with('success', 'Announcement has been posted.');
        }else{
            return redirect()->back()->with('error', 'Announcement failed to post.');
        }
    }
}

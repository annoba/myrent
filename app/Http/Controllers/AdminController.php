<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Landlord;
use App\SubsRequest;
use App\Admin;
use App\Room;

class AdminController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function client(){
           $data=Landlord::join('client_building','landlord_profile.building_id','=','client_building.id')
                                    ->select('landlord_profile.id','first_name','last_name','landlord_profile.contact_number','gender','birthday','client_building.name')->get();
        return view('admin.client')->with('data',$data);
    }

    public function addclient(){
        return view('admin.client_add');
    }

    public function subsView(){
        $data=SubsRequest::subsView();
        return view('admin.clientsubs')->with('data',$data);
    }
    public function subsViewPending(){
        $data=SubsRequest::subsViewPending();
        return view('admin.clientsubspending')->with('data',$data);
    }

    public function acceptPending(Request $request){
        try{
            if($request->action=='accept'){
                $data=0;
            }else{
                $data=2;
            }
            $update=SubsRequest::where('id',$request->id)->update(['status'=>$data]);
            $updatev2=Room::join('subscription_request','building_rooms.id','=','subscription_request.room_id')
                            ->where('subscription_request.id',$request->id)->update(['building_rooms.isPremium'=>$data]);
            if($update && $updatev2){
                return 'success';
            }
            else{
                return 'error';
            }
        }catch(Exception $e){
            return back()->withErrors($e);
        }
    }

    public function adminAccounts(){
        return view('admin.adminaccounts')->with('data',Admin::adminAccountLists());
        // return Admin::adminAccountLists();
    }
    public function addAccount($id=null){
        return view('admin.adminadd');
    }

    public function insertNewAccount(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'Firstname'=>'required|max:50',
                'Lastname'=>'required|max:50',
                'contnum'=>'required',
                'username'=>'required|max:20|unique:users,username',
                'mail'=>'required|email|max:30|unique:users,email',
                'password'=>'required|max:20|confirmed',
            ]);

            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator->errors());
            }else{
                return Admin::addAccount($request);
            }
        } catch (Exception $e) {
            return back()->withErrors($e);
        } 
    }

    public function insertClient(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'Firstname'=>'required|max:50',
                'Lastname'=>'required|max:50',
                'contnum'=>'required',
                'gender'=>'required',
                'bdate'=>'required',
                'buildingtype'=>'required',
                'building_name'=>'required|unique:client_building,name',
                'building_address'=>'required',
                'build_contnum'=>'required',
                'username'=>'required|max:20|unique:users,username',
                'mail'=>'required|email|max:30|unique:users,email',
                'password'=>'required|max:20|confirmed',
                
            ]);

            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator->errors());
            }else{
                return Landlord::addnewclient($request);
            }
        } catch (Exception $e) {
            return back()->withErrors($e);
        } 
    }

}

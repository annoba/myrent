<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Admin;
use App\Announcement;
use App\Tenant;
use App\Landlord;
use App\Room;
use App\Building;
use App\Messages;
use App\Bill;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('welcome');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user()->user_type==1)
        {
            return redirect(route('admin.client'));
        }else if(Auth::user()->user_type==2)
        {
            $announcement = Announcement::where('postedBy', Auth::user()->landlord_profile->id)
            ->orderBy('created_at', 'desc')
            ->take(10)
            ->get();
            return view('landlord.home')->with('announcements', $announcement);
        }else if(Auth::user()->user_type==3)
        {   
            return view('tenant.home')->with('data',Announcement::join('landlord_profile','postedBy','=','landlord_profile.id')
                                                                ->join('building_rooms','landlord_profile.building_id','=','building_rooms.building_id')
                                                                ->select('announcement.created_at','title','body','level_of_urgency')
                                                                ->where('building_rooms.id',auth()->user()->tenant_profile->room_id)->take(4)->orderBy('announcement.id','desc')->get());
        }
        // Announcement::take(4)->orderBy('id','desc')->get()
    }
    public function welcome()
    {
        if(Auth::check()){
            return $this->index();
        }else{
            return view('welcome');
        }
    }
    public function Profile(){
        if(auth()->user()->user_type==1){
            return view('admin.adminprofile')->with('data',Admin::adminAccountLists(auth()->user()->id));
        }else if(auth()->user()->user_type==2){
            return view('landlord.landlordprofile')->with('data',Landlord::landlordAccountList(auth()->user()->id));
        }else if(auth()->user()->user_type==3){
            return view('tenant.tenantprofile')->with('data',Tenant::tenantAccountList(auth()->user()->id));
            // return Tenant::tenantAccountList(auth()->user()->id);
        }
    }

    public function gotoRecords(){
        $data = array();
        $rooms = array();
        $id = Auth::user()->landlord_profile->building_id;
        $tenants = array();
        
        foreach(Room::where([['building_id','=',$id],['isActive','=',1]])->get() as $room){
            if(Tenant::where([['room_id','=',$room->id]])->count() != 0){
                foreach(Tenant::where([['room_id','=',$room->id]])->get() as $tenant){
                    $isActive = $tenant->user->isActive;
                    if($isActive == "1"){
                        $tenants[] = $tenant;
                    }
                }
            }else{
                $tenants = [];
            }
            $rooms[] = array(
                'room_name' => $room->room_number,
                'isPremium' => $room->isPremium,
                'max_tenants' => $room->max_tenants,
                'id' => $room->id,
                'curr_tenants' => count($tenants),
            );
        }

        $id = Auth::user()->landlord_profile->building_id;
        $tenants = array();
        foreach(Building::where('id','=',$id)->get() as $building) {
            foreach($building->rooms->all() as $room){
                foreach($room->tenant->all() as $tenant){
                    if($tenant->user->isActive == 1){
                        $tenants[] = $tenant;
                    }
                }
            } 
        }

        $id = Auth::user()->landlord_profile->building_id;
        $bills = array();
        foreach(Room::where('building_id','=',$id)->get() as $room) {
            foreach(Bill::where([['status','=','Pending'],['billed_to','=',$room->id]])->get() as $bill){
                $bills[] = $bill;
            } 
        }

        $data = [
            'rooms' =>$rooms,
            'tenants' =>$tenants,
            'bills' => $bills,
        ];
        // return $data;

        return view('landlord.records')->with('data',$data);

    }
    public function getReceiveMessage(Request $req){
        if(auth()->user()->user_type==2){
            return Messages::getReceiveMessage($req->id,"tenant");
        }else{
            return Messages::getReceiveMessage($req->id,"landlord");
        }
    }

    
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Room;
use App\Tenant;
use App\Building;
use App\Landlord;
use App\Messages;
use Auth;
use Validator;

class LandlordController extends Controller
{
    public function gotoRooms(){
        $id = Auth::user()->landlord_profile->building_id;
        $rooms = Room::where('building_id','=',$id)->get();

        return view('landlord.room')->with('rooms',$rooms);
    }

    public function editProfile(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'Firstname'=>'required|max:50',
                'Lastname'=>'required|max:50',
                'contnum'=>'required',
                'username'=>'required|max:20|unique:users,username,'.auth()->user()->id,
                'mail'=>'required|email|max:30|unique:users,email,'.auth()->user()->id,
                // 'bdate'=>'required',
                'gender'=>'required',
            ]);

            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator->errors());
            }else{
                return Landlord::updateLandlord($request);
            }
        }catch(Exeption $e){
            return back()->withErrors($e);
        }
    
    }

    public function getAllUsersRoomRedirect(){
        $data=Messages::getAllUsersRoom(null);
        return redirect('/message/landlord/'.$data['tenants']->first()->user_id);
    }
    public function getAllUsersRoom($id){
        return view('landlord.message')->with('rooms',Messages::getAllUsersRoom($id));   

    }

    public function gotoTenants(){
        $id = Auth::user()->landlord_profile->building_id;
        $data = array();
        foreach(Building::where('id','=',$id)->get() as $building) {
            foreach($building->rooms->all() as $room){
                foreach($room->tenant->all() as $tenant){
                    if($tenant->user->isActive == 1){
                        $data[] = $tenant;
                    }
                }
            } 
        }
        return view('landlord.tenant')->with('tenants',$data);
    }
    public function getMyMessage(Request $request){
        return Messages::getMyMessage($request->id);//user_ID
    }
    

    public function sendMessage(Request $req){
        if(Messages::sendMessage($req)){
            return 'success';
        }else{
            return 'fail';
        }
        // return $req;
    }
}

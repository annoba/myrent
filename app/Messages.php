<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Room;

class Messages extends Model
{
    protected $table = 'table_room_message';

    public function user(){
        $this->hasMany('App\User');
    }
    public function rooms(){
        $this->hasMany('App\Room');
    }

    public static function getMyMessage($id){
        return Messages::where('tenant_userid',$id)->get();
    }
    public static function getReceiveMessage($data,$type){
        return Messages::where('tenant_userid',$data)
                        ->where('sender',$type)->get();
        // return $data;
    }

    public static function getAllUsersRoom($id){
            if(auth()->user()->user_type==2){
                $rooms=Room::where('building_id',auth()->user()->landlord_profile->building_id)->get();
                $ids=array();
                foreach($rooms as $room){
                    array_push($ids,$room->id);
                }
                $tenants=Tenant::whereIn('room_id',$ids)->get();
                if($id==null){
                   $find=$tenants->first()->user_id;
                }else{
                    $find=$id;
                }
                $data=[
                    'tenants'=>$tenants,
                    'message'=>Messages::getMyMessage($find),
                    'id'=>$find,
                ];

                return  $data;
                                // return $ids;
            }
        
    }

    public static function sendMessage($data){
        try{
            $ms=new self;
            $ms->tenant_userid=$data->tID;
            $ms->room_id=$data->rID;
            $ms->message=$data->msg;
            $ms->sender=$data->sender;

            $ms->save();
            return true;
        }catch(Exception $e){
            return false;
        }
    }

   
}

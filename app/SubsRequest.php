<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubsRequest extends Model
{
    protected $table = "subscription_request";

    public function landlord(){
        return $this->belongsTo('App\Landlord');
    }

    public function room(){
        return $this->belongsTo('App\Room');
    }

    // landlord->subs->rooms all
    public static function subsView(){
        return SubsRequest::join('landlord_profile','subscription_request.landlord_id','=','landlord_profile.id')
                            ->join('building_rooms','subscription_request.room_id','=','building_rooms.id')->where('status','<=','1')->get();
    }

    // landlord->subs->rooms pending
    public static function subsViewPending(){
        return SubsRequest::join('landlord_profile','subscription_request.landlord_id','=','landlord_profile.id')
                            ->join('building_rooms','subscription_request.room_id','=','building_rooms.id')->where('status',1)->get();
    }

}

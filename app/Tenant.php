<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tenant extends Model
{
    protected $table = 'tenant_profile';

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function room() {
        return $this->belongsTo('App\Room');
    }

    public function bill() {
        return $this->hasMany('App\BillPerTenant','id');
    }

    public static function tenantAccountList($id=null){
        if($id==null){
            return Tenant::join('users','tenant_profile.user_id','=','users.id')
                        ->select('tenant_profile.id','first_name','last_name','username','email','contact_number','gender','birthday','start_date','end_date','avatar')
                        ->get();   
        }
        else{
            return Tenant::join('users','tenant_profile.user_id','=','users.id')
                        ->select('tenant_profile.id','first_name','last_name','username','email','contact_number','gender','birthday','start_date','end_date','avatar')
                        ->where('users.id',$id)
                        ->get();  
        }
    }

    public static function updateTenant($data){
        $tenant_data=Tenant::where('user_id',auth()->user()->id)
                            ->get()->first();
        $user_data=User::find(auth()->user()->id);

        $tenant_data->first_name=$data->Firstname;
        $tenant_data->last_name=$data->Lastname;
        $tenant_data->contact_number=$data->contnum;
        $tenant_data->birthday=$data->bdate;
        $tenant_data->start_date=$data->sdate;
        $tenant_data->end_date=$data->edate;
        $tenant_data->gender=$data->gender;

        $user_data->username=$data->username;
        $user_data->email=$data->mail;

        $user_data->save();
        $tenant_data->save();

        return back()->with('success', 'Successfully updated profile');
        // return $current_data;
    }
}

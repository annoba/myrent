<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Building;
use App\User;

class Landlord extends Model
{
    protected $table = 'landlord_profile';

    public function user() {
        return $this->belongsTo('App\User');
    }
    public function request(){
        return $this->hasMany('App\Request');
    }
    public function notification(){
        return $this->hasMany('App\Announcement');
    }

    public static function updateLandlord($data){
        $landlord_data=Landlord::where('user_id',auth()->user()->id)
                            ->get()->first();
        $user_data=User::find(auth()->user()->id);

        $landlord_data->first_name=$data->Firstname;
        $landlord_data->last_name=$data->Lastname;
        $landlord_data->contact_number=$data->contnum;
        $landlord_data->birthday=$data->bdate;
        $landlord_data->gender=$data->gender;

        $user_data->username=$data->username;
        $user_data->email=$data->mail;

        $user_data->save();
        $landlord_data->save();

        return back()->with('success', 'Successfully updated profile');
        // return $landlord_data;
    }
    
    public static function landlordAccountList($id=null){
        if($id==null){
            return Landlord::join('users','landlord_profile.user_id','=','users.id')
                        ->select('landlord_profile.id','first_name','last_name','username','email','contact_number','gender','birthday','avatar')
                        ->get();   
        }
        else{
            return Landlord::join('users','landlord_profile.user_id','=','users.id')
                        ->select('landlord_profile.id','first_name','last_name','username','email','contact_number','gender','birthday','avatar')
                        ->where('users.id',$id)
                        ->get();  
        }
    }

    public static function addnewclient($data)
    {
        try {
            // building
            if(isset($data->building_name)){
                $b=new Building;
                $b->name=$data->building_name;
                $b->address=$data->building_address;
                $b->contact_number=$data->build_contnum;
                $b->building_type=$data->buildingtype;
                $bsave=$b->save();
            }
            else{
                $bid=User::where('id',auth()->user()->id)
                            ->join('landlord_profile','users.id','=','user_id')
                            ->select('building_id')
                            ->get();
                $bsave=true;
            }
            // useraccount
            $u=new User;
            $u->email=$data->mail;
            $u->username=$data->username;
            $u->password=bcrypt($data->password);
            $u->user_type=2;
            $u->isActive=1;
            $u->avatar = 'default.png';
            $usave=$u->save();
            // landlord
            $ll = new self; 
            $ll->first_name=$data->Firstname;
            $ll->last_name = $data->Lastname;
            $ll->contact_number = $data->contnum;
            $ll->gender = $data->gender;
            $ll->birthday = $data->bdate;

            if(isset($data->building_name)){
                $ll->building_id=$b->id;
            }
            else{
                $ll->building_id=$bid;
            }
           
            $ll->user_id=$u->id;
            $lsave=$ll->save();
           
            if($usave==true && $lsave==true && $bsave==true){
                return back()->with('success', 'Successfully added client');
            }else{
                return back()->with('error', 'Error on Inserting user');
            }
            
        } catch (Exception $e) {
            return back()->with('error',$e->getMessage()); 
        }
    }

    public function building() {
        return $this->belongsTo('App\Building');
    }
}

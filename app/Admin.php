<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Admin extends Model
{
    protected $table = 'admin_profile';

    public function user() {
        return $this->belongsTo('App\User');
    }

    public static function adminAccountLists($id=null){
        if($id==null){
            return Admin::join('users','admin_profile.user_id','=','users.id')
                        ->select('admin_profile.id','first_name','last_name','username','email','contact_number','added_by','isActive','avatar')
                        ->get();   
        }
        else{
            return Admin::join('users','admin_profile.user_id','=','users.id')
                        ->select('admin_profile.id','first_name','last_name','username','email','contact_number','added_by','isActive','avatar')
                        ->where('users.id',$id)
                        ->get();  
        }
    }

    public static function addAccount($data)
    {
        try {
            // useraccount
            $u=new User;
            $u->email=$data->mail;
            $u->username=$data->username;
            $u->password=bcrypt($data->password);
            $u->user_type=1;
            $u->isActive=1;
            $u->avatar ='default.png';
            $usave=$u->save();
            // landlord
            $admin = new self; 
            $admin->first_name=$data->Firstname;
            $admin->last_name = $data->Lastname;
            $admin->contact_number = $data->contnum;
            $admin->user_id=$u->id;
            $admin->added_by=auth()->user()->id;
            $adminsave=$admin->save();

            if($usave==true && $adminsave==true){
                return back()->with('success', 'Successfully added admin');
            }else{
                return back()->with('error', 'Error on Inserting user');
            }
            // return 123;
            
        } catch (Exception $e) {
            return back()->with('error',$e->getMessage()); 
        }
    }
}

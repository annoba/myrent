<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    protected $table = "bill_per_room";

    public function room() {
        return $this->belongsTo('App\Room', 'billed_to');
    }

    public function billing_type() {
        return $this->belongsTo('App\BillType', 'bill_type');
    }

    public function bill_per_tenant() {
        return $this->hasMany('App\BillPerTenant', 'id');
    }
}

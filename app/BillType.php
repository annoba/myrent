<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillType extends Model
{
    protected $table = 'bills_type';

    public function bill() {
        return $this->hasMany('App\Bill');
    }

    public function bill_per_tenant() {
        return $this->hasMany('App\BillPerTenant','id');
    }
}

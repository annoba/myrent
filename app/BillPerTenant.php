<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillPerTenant extends Model
{
    protected $table = 'bill_per_tenant';

    public function tenant() {
        return $this->belongsTo('App\Tenant','billed_to');
    }

    public function type() {
        return $this->belongsTo('App\BillType','bill_type');
    }

    public function bill() {
        return $this->belongsTo('App\Bill','bill_per_room_id');
    }
}

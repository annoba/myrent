<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table = "building_rooms";

    public function building() {
        return $this->belongsTo('App\Building');
    }

    public function landlord(){
        return $this->hasMany('App\Landlord');
    }

    public function tenant() {
        return $this->hasMany('App\Tenant');
    }

    public function bill() {
        return $this->hasMany('App\Bill', 'id');
    }
}

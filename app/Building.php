<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Building extends Model
{
    protected $table = "client_building";

    public function rooms() {
        return $this->hasMany('App\Room');
    }

    public function landlord() {
        return $this->hasMany('App\Landlord');
    }
}
